CREATE DATABASE  IF NOT EXISTS `urbantaxi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `urbantaxi`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: urbantaxi
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `municipio`
--

DROP TABLE IF EXISTS `municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipio` (
  `id_Municipio` int(11) NOT NULL AUTO_INCREMENT,
  `Municipio` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Municipio`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipio`
--

LOCK TABLES `municipio` WRITE;
/*!40000 ALTER TABLE `municipio` DISABLE KEYS */;
INSERT INTO `municipio` VALUES (1,'Libertador'),(2,'Alto Orinoco'),(3,'Atabapo'),(4,'Atures'),(5,'Autana'),(6,'Manapiare'),(7,'Maroa'),(8,'Río Negro'),(9,'Anaco'),(10,'Aragua'),(11,'Bolívar'),(12,'Bruzual'),(13,'Cajigal'),(14,'Carvajal'),(15,'Diego Bautista Urbaneja'),(16,'Freites'),(17,'Guanipa'),(18,'Guanta'),(19,'Independencia'),(20,'Libertad'),(21,'McGregor'),(22,'Miranda'),(23,'Monagas'),(24,'Peñalver'),(25,'Píritu'),(26,'San Juan de Capistrano'),(27,'Santa Ana'),(28,'Simón Rodriguez'),(29,'Sotillo'),(30,'Achaguas'),(31,'Biruaca'),(32,'Muñoz'),(33,'Páez'),(34,'Pedro Camejo'),(35,'Rómulo Gallegos'),(36,'San Fernando'),(37,'Camatagua'),(38,'Francisco Linares Alcántara'),(39,'Girardot'),(40,'José Ángel Lamas'),(41,'José Félix Ribas'),(42,'José Rafael Revenga'),(43,'Mario Briceño Iragorry'),(44,'Ocumare de la Costa de Oro'),(45,'San Casimiro'),(46,'San Sebastián'),(47,'Santiago Mariño'),(48,'Santos Michelena'),(49,'Sucre'),(50,'Tovar'),(51,'Urdaneta'),(52,'Zamora'),(53,'Alberto Arvelo Torrealba'),(54,'Andrés Eloy Blanco'),(55,'Antonio José de Sucre'),(56,'Arismendi'),(57,'Barinas'),(58,'Cruz Paredes'),(59,'Ezequiel Zamora'),(60,'Obispos'),(61,'Pedraza'),(62,'Rojas'),(63,'Sosa'),(64,'Caroní'),(65,'Cedeño'),(66,'El Callao'),(67,'Gran Sabana'),(68,'Heres'),(69,'Piar'),(70,'Raúl Leoni'),(71,'Roscio'),(72,'Sifontes'),(73,'Padre Pedro Chen'),(74,'Bejuma'),(75,'Carlos Arvelo'),(76,'Diego Ibarra'),(77,'Guacara'),(78,'Juan José Mora'),(79,'Los Guayos'),(80,'Montalbán'),(81,'Naguanagua'),(82,'Puerto Cabello'),(83,'San Diego'),(84,'San Joaquín'),(85,'Valencia'),(86,'Anzoátegui'),(87,'Falcón'),(88,'Lima Blanco'),(89,'Pao de San Juan Bautista'),(90,'Ricaurte'),(91,'Antonio Díaz Curiapo'),(92,'Casacoima'),(93,'Pedernales'),(94,'Tucupita'),(95,'Acosta'),(96,'Buchivacoa'),(97,'Cacique Manaure'),(98,'Carirubana'),(99,'Colina'),(100,'Dabajuro'),(101,'Democracia'),(102,'Federación'),(103,'Jacura'),(104,'Los Taques'),(105,'Mauroa'),(106,'Monseñor Iturriza'),(107,'Palmasola'),(108,'Petit'),(109,'San Francisco'),(110,'Silva'),(111,'Tocópero'),(112,'Unión'),(113,'Urumaco'),(114,'Esteros de Camaguan'),(115,'Chaguaramas'),(116,'El Socorro'),(117,'Francisco de Miranda'),(118,'José Tadeo Monagas'),(119,'Juan Germán Roscio'),(120,'Julián Mellado'),(121,'Las Mercedes'),(122,'Leonardo Infante'),(123,'Pedro Zaraza'),(124,'Ortíz'),(125,'San Gerónimo de Guayabal'),(126,'San José de Guaribe'),(127,'Santa María de Ipire'),(128,'Crespo'),(129,'Iribarren'),(130,'Jiménez'),(131,'Morán'),(132,'Palavecino'),(133,'Simón Planas'),(134,'Torres'),(135,'Alberto Adriani'),(136,'Andrés Bello'),(137,'Antonio Pinto Salinas'),(138,'Tulio Febres Cordero'),(139,'Zea'),(140,'Acevedo'),(141,'Baruta'),(142,'Brión'),(143,'Buroz'),(144,'Carrizal'),(145,'Chacao'),(146,'Cristóbal Rojas'),(147,'El Hatillo'),(148,'Guaicaipuro'),(149,'Lander'),(150,'Los Salias'),(151,'Paz Castillo'),(152,'Pedro Gual'),(153,'Plaza'),(154,'Simón Bolívar'),(155,'Aguasay'),(156,'Maturín'),(157,'Punceres'),(158,'Santa Bárbara'),(159,'Sotillo'),(160,'Uracoa'),(161,'Antolín del Campo'),(162,'Díaz'),(163,'García'),(164,'Gómez'),(165,'Maneiro'),(166,'Marcano'),(167,'Mariño'),(168,'Península de Macanao'),(169,'Tubores'),(170,'Villalba'),(171,'Agua Blanca'),(172,'Araure'),(173,'Esteller'),(174,'Guanare'),(175,'Guanarito'),(176,'Monseñor José Vicenti de Unda'),(177,'Ospino'),(178,'Papelón'),(179,'San Genaro de Boconoíto'),(180,'San Rafael de Onoto'),(181,'Santa Rosalía'),(182,'Turén'),(183,'Andrés Mata'),(184,'Benítez'),(185,'Bermúdez'),(186,'Cajigal'),(187,'Cruz Salmerón Acosta'),(188,'Mejía'),(189,'Montes'),(190,'Ribero'),(191,'Valdez'),(192,'Antonio Rómulo Costa'),(193,'Ayacucho'),(194,'Cárdenas'),(195,'Córdoba'),(196,'Fernández Feo'),(197,'García de Hevia'),(198,'Guásimos'),(199,'Jáuregui'),(200,'José María Vargas'),(201,'Junín'),(202,'San Judas Tadeo'),(203,'Libertad'),(204,'Lobatera'),(205,'Michelena'),(206,'Panamericano'),(207,'Pedro María Ureña'),(208,'Rafael Urdaneta'),(209,'Samuel Dario Maldonado'),(210,'San Cristóbal'),(211,'Seboruco'),(212,'Simón Rodríguez'),(213,'Torbes'),(214,'Uribante'),(215,'Boconó'),(216,'Candelaria'),(217,'Carache'),(218,'Escuque'),(219,'José Felipe Márquez Cañizalez'),(220,'Juan Vicente Campos Elías'),(221,'La Ceiba'),(222,'Pampán'),(223,'Trujillo'),(224,'Andres Linares'),(225,'Pampanito'),(226,'Vargas'),(227,'Arístides Bastidas'),(228,'Bruzual'),(229,'Cocorote'),(230,'Almirante Padilla'),(231,'Baralt'),(232,'Cabimas'),(233,'Catatumbo'),(234,'Colón'),(235,'Francisco Javier Pulgar'),(236,'Jesús Enrique Losada'),(237,'Jesús María Semprún');
/*!40000 ALTER TABLE `municipio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-15 22:12:20
