package Modelo;

//import Vista.VistaIngresar; Lo usar� en la evaluacion 4
public class Mod {

	//Variables
	private static String NombreCompleto;
	private static String Dir;
	private static String TLF;
	private static String CorreoElec;
	private static int estado;
	private static int municipio;
	private static String placaVE,modeloVE,marcaVE;
	private static int Ci, ci2;
	
	//public VistaIngresar Vingresar; Lo usar� en la evaluacion 4
	
	public Mod() {
		//Inicializar los datos
		
		Mod.NombreCompleto = "";
		Mod.Dir = "";
		Mod.TLF = "";
		Mod.CorreoElec = "";
		Mod.placaVE = "";
		Mod.modeloVE = "";
		Mod.marcaVE = "";
	}
	
	//Setters
	public void setNombreCompleto(String Nombre) { NombreCompleto = Nombre; }
	public void setCi(String string) { Ci = Integer.parseInt(string);}
	public void setDir(String dir) { Dir = dir;}
	public void setTLF(String tlf) { TLF = tlf; }
	public void setCorreoElec(String correo) { CorreoElec = correo;}
	public void setestado(int Estado) { estado = Estado; }
	public void setmunicipio(int Municipio) { municipio = Municipio;}
	public void setplacaVE(String placavehiculo) { placaVE = placavehiculo;}
	public void setmodeloVE(String modelovehiculo) { modeloVE = modelovehiculo;}
	public void setmarcaVE(String marcavehiculo) {marcaVE = marcavehiculo;}
	public void setci2(String c) { ci2 = Integer.parseInt(c);}
	
	//Getters
	public String getNombreCompleto() {return NombreCompleto;}
	public int getCi() {return Mod.Ci;}
	public String getDir() {return Dir;}
	public String getTLF() {return TLF;}
	public String getCorreoElec() {return CorreoElec;}
	public int getestado() {	return estado;}
	public int getmunicipio() {return municipio;}
	public String getplacaVE() {return placaVE;}
	public String getmodeloVE() {return modeloVE;}
	public String getmarcaVE() {return marcaVE;}
	public int getci2() {return ci2;}
	
	public void Limpiar() {
		NombreCompleto = null;
		Ci = 0;
		Dir = null;
		TLF = null;
		CorreoElec = null;
		estado = 0;
		municipio = 0;
		placaVE = null;
		modeloVE = null;
		marcaVE = null;
		ci2 = 0;
	}
}