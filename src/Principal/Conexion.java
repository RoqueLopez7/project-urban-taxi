package Principal;

import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.*;

import javax.swing.JOptionPane;

import Modelo.Mod;
import Vista.Vista;
//import Vista.VistaIngresar;
//import Controlador.ControladorIngresar;
//import Controlador.ControladorPrincipal;

import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class Conexion {

	//Caracteristicas necesarias para conectar con la base de datos
	Connection conexion; //Conectar con la base de datos
	Statement stt; //Para el use de la base de datos
	PreparedStatement PSChofer = null, PSVehiculo = null, PSEstado = null, PSMunicipio = null, Mistatement; //Para las instruccion SQL
	
	public Vista Ventana;
	
	//VistaIngresar Vin; lo usare en la evaluacion 4
	public String Datos;
	public int cedula, cedulaBor; //CedulaBor es una variable exclusiva para el segmento borrar
	public int  idvehiculo = 0, idestado = 0, idmunicipio = 0, idve = 0; //Variables para tomar los id de dichas tablas
	private ResultSet Chofer,Vehiculo,Estado,Municipio; //Un ResultSet para cada tabla
	
	public Conexion() {

	}
	
	//Metodo para la insercion
	public void Insercion(Mod Mode) {

		String InChofer = "INSERT INTO chofer (`cedula`, `Nombre_Completo`, `direccion`, `Telefono`, `Correo_Electronico`, `ID_Estado`, `ID_Municipio`, `ID_Vehiculo`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
		String InVehiculo = "INSERT INTO  vehiculo (`Placa_Vehiculo`, `Modelo_Vehiculo`, `Marca_Vehiculo`) values(?,?,?);";
		try {
			
		//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

		conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
		//2. Creacion del Statement
			
		stt = conexion.createStatement();
		
		//Usar una base de datos
		
		String use = "use urbantaxi;"; //Instruccion SQL
		
		stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
		
		//Vehiculo
		PSVehiculo = conexion.prepareStatement(InVehiculo);
		PSVehiculo.setString(1, Mode.getplacaVE());
		PSVehiculo.setString(2, Mode.getmodeloVE());
		PSVehiculo.setString(3, Mode.getmarcaVE());
		PSVehiculo.execute();	
	
		//Chofer
		PSChofer = conexion.prepareStatement(InChofer);
		PSChofer.setInt(1, Mode.getCi());
		PSChofer.setString(2, Mode.getNombreCompleto());
		PSChofer.setString(3, Mode.getDir());
		PSChofer.setString(4, Mode.getTLF());
		PSChofer.setString(5, Mode.getCorreoElec());
		PSChofer.setInt(6, Mode.getestado());
		PSChofer.setInt(7, Mode.getmunicipio());
		PSChofer.setInt(8, IDVE(Mode));
		PSChofer.execute();
		
		//Informe sobre el proceso concluido
		JOptionPane.showMessageDialog(null,"Proceso concluido con �xito.");
		
		} catch(SQLException e) {

			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"NO CONECTA CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}
	}
	
	//Metodo para la busqueda

	public void Busqueda(Mod Mode) {
	//Conectar con la base de datos
	
		try {
			
			//1. Conectar con la base de datos
			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			
			//En el parentesis se debe ingresar una sentencia sql
			PSChofer = conexion.prepareStatement("SELECT * FROM chofer WHERE cedula =?;");
			
			//Se busca en todas las tablas los datos del chofer por su id
			PSVehiculo = conexion.prepareStatement("SELECT * FROM vehiculo WHERE ID_Vehiculo =?;");
			PSEstado = conexion.prepareStatement("SELECT * FROM estado WHERE ID_Estado =?;");
			PSMunicipio = conexion.prepareStatement("SELECT * FROM municipio WHERE ID_Municipio =?;");
			
			//Ingresar los campos de ?
			
			PSChofer.setInt(1,Mode.getCi());
			PSVehiculo.setInt(1, IDvehiculo(Mode));
			PSEstado.setInt(1, IDestado(Mode));
			PSMunicipio.setInt(1, IDmunicipio(Mode));
			
			//Se crean los ResultSets
			
			Chofer = PSChofer.executeQuery(); 
			Vehiculo = PSVehiculo.executeQuery();
			Estado = PSEstado.executeQuery();
			Municipio = PSMunicipio.executeQuery();
			//Recorrer el ResultSet
			
			while(Chofer.next() && Vehiculo.next() && Estado.next() && Municipio.next()) {
				//Guardar todos los datos de la persona para ser retornado
				Datos = "-----------------------------------------------------------------------------\n"+
						"Nombre y Apellidos: "+Chofer.getString("nombre_completo")+".\n"+"Cedula: "+Chofer.getInt("cedula")+".\n"+"Direccion: "+Chofer.getString("direccion")+".\n"+
						"Telefono: "+Chofer.getString("telefono")+".\n"+"Correo: "+Chofer.getString("correo_electronico")+".\n"+"Estado: "+Estado.getString("Estado")+".\n"+
						"Municipio: "+Municipio.getString("Municipio")+".\n"+"Placa del vehiculo: "+Vehiculo.getString("Placa_Vehiculo")+".\n"+"Modelo del vehiculo: "+Vehiculo.getString("Modelo_Vehiculo")+".\n"+
						"Marca del vehiculo: "+Vehiculo.getString("Marca_Vehiculo")+".\n"+
						"-----------------------------------------------------------------------------\n";
				cedula = Chofer.getInt("cedula");
				cedulaBor = Chofer.getInt("cedula");
			}		
		}catch(SQLException e) {
			
			//Por si no agarra
			JOptionPane.showMessageDialog(null, "Hubo un problema en la consulta.\nVerifique la cedula del conductor.");
			
			e.printStackTrace();
		}
	}
	
	//Metodo para la consulta general
	public void ConsultaG(Mod Mode) {
		
		try {
			
			//1. Conectar con la base de datos
			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//En el parentesis se debe ingresar una sentencia sql
			PSChofer = conexion.prepareStatement("SELECT * FROM chofer");
			
			//Se busca en todas las tablas los datos del chofer por su id
			PSVehiculo = conexion.prepareStatement("SELECT * FROM vehiculo");
			PSEstado = conexion.prepareStatement("SELECT * FROM estado");
			PSMunicipio = conexion.prepareStatement("SELECT * FROM municipio");
			
			//Se crean los ResultSets
			
			Chofer = PSChofer.executeQuery(); 
			Vehiculo = PSVehiculo.executeQuery();
			Estado = PSEstado.executeQuery();
			Municipio = PSMunicipio.executeQuery();
			//Recorrer el ResultSet
			
			while(Chofer.next() && Vehiculo.next() && Estado.next() && Municipio.next()) {
				//Para informar que termin� la n�mina
				if(Datos == null) { Datos = "";}
				
				//Guardar todos los datos de la persona para ser retornado
				Datos = "-----------------------------------------------------------------------------\n"+
						"Nombre y Apellidos: "+Chofer.getString("nombre_completo")+".\n"+"Cedula: "+Chofer.getInt("cedula")+".\n"+"Direccion: "+Chofer.getString("direccion")+".\n"+
						"Telefono: "+Chofer.getString("telefono")+".\n"+"Correo: "+Chofer.getString("correo_electronico")+".\n"+"Estado: "+Estado.getString("Estado")+".\n"+
						"Municipio: "+Municipio.getString("Municipio")+".\n"+"Placa del vehiculo: "+Vehiculo.getString("Placa_Vehiculo")+".\n"+"Modelo del vehiculo: "+Vehiculo.getString("Modelo_Vehiculo")+".\n"+
						"Marca del vehiculo: "+Vehiculo.getString("Marca_Vehiculo")+".\n"+
						"-----------------------------------------------------------------------------\n"+
						Datos;
			}			
		}catch(SQLException e){
			
			//Indica donde esta el problema
			e.printStackTrace();
			
			//Mensaje de advertencia
			JOptionPane.showMessageDialog(null, "Hubo un problema en la consulta.\nVerifique la cedula del conductor.");
		}
	}

	//Metodo para la actualizacion
	public void Actualizacion(Mod Mode) {
		try {
			
			//1. Conectar con la base de datos
			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//Se establece el comando SQL
			
			//Vehiculo
			PSVehiculo = conexion.prepareStatement("UPDATE vehiculo set Placa_Vehiculo =?, Modelo_Vehiculo = ?, Marca_Vehiculo = ? where id_Vehiculo =?");
			PSVehiculo.setString(1, Mode.getplacaVE());
			PSVehiculo.setString(2, Mode.getmodeloVE());
			PSVehiculo.setString(3, Mode.getmarcaVE());
			PSVehiculo.setInt(4, IDvehiculo(Mode));
			PSVehiculo.execute();
			
			//Chofer
			PSChofer = conexion.prepareStatement("UPDATE chofer set cedula =?, nombre_completo =?, direccion =?, telefono =?, correo_electronico =?, ID_Estado =?, ID_Municipio =?, ID_Vehiculo =? where cedula =?");
			PSChofer.setInt(1, Mode.getCi());
			PSChofer.setString(2, Mode.getNombreCompleto());
			PSChofer.setString(3, Mode.getDir());
			PSChofer.setString(4, Mode.getTLF());
			PSChofer.setString(5, Mode.getCorreoElec());
			PSChofer.setInt(6, Mode.getestado());
			PSChofer.setInt(7, Mode.getmunicipio());
			PSChofer.setInt(8, IDVE(Mode));
			PSChofer.setInt(9, Mode.getci2());
			PSChofer.execute();
			JOptionPane.showMessageDialog(null, "Datos actualizados.");
		}catch(SQLException e) {
			//Mostrar donde esta el error y mensaje de ayuda por si ocurre algo
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Hubo un problema en la actualizacion de los datos.\nVerifica los datos o la conexion de la base de datos.");
		}
	}

	//Metodo para la eliminacion
	public void Borrar(Mod Mode) {
		
		try {
			//1. Conectar con la base de datos
			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//Eliminacion
			
			PSChofer = conexion.prepareStatement("DELETE FROM chofer WHERE cedula = ?;");
			PSChofer.setInt(1, Mode.getCi());
			PSChofer.execute();
			
			PSChofer = conexion.prepareStatement("DELETE FROM vehiculo WHERE ID_Vehiculo = ?;");
			PSChofer.setInt(1, IDvehiculo(Mode));
			PSChofer.execute();
			
			//Mensaje que el proceso est� concluido
			JOptionPane.showMessageDialog(null, "Proceso Concluido.\nConductor eliminado.");
		}catch(SQLException e) {
			//Mensaje de advertencia
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Hubo un problema en la actualizacion de los datos.\\nVerifica la cedula ingresada o la conexion de la base de datos.");
		}
	}
	
	//metodo para generar el reporte
	public void Reporte() {
		//Creamos componente de tipo document
		Document documento = new Document(PageSize.LETTER, 50, 50, 50, 50);
		
		try {//Creamos el try
			
			//Creamos un par de metadatos
			documento.addAuthor("Roque Lopez");
			documento.addTitle("Reporte PDF");
		    
			String ruta = System.getProperty("user.home"); //Creamos la variable el cual tendr� el acceso a la ruta
			PdfWriter.getInstance(documento, new FileOutputStream(ruta+"/Desktop/Reporte.pdf")); //Creamos el PDFWriter para indicar donde se guardar� el pdf 
		    
			documento.open(); //Abrimos el documento
		      
			PdfPTable tablachofer1 = new PdfPTable(4); //Se crea la tabla que ser� plasmada en el pdf
			PdfPTable tablachofer2 = new PdfPTable(4);
			PdfPTable tablachofer3 = new PdfPTable(1);
			PdfPTable tablaestado = new PdfPTable(2);
			PdfPTable tablamunicipio = new PdfPTable(2);
			PdfPTable tablavehiculo = new PdfPTable(4);
			
			
			//Colocamos el nombre a cada columna de la tabla chofer
			tablachofer1.addCell("C�digo.");
			tablachofer1.addCell("Cedula.");
			tablachofer1.addCell("Nombres y Apellidos.");
			tablachofer1.addCell("Direcci�n.");
			tablachofer2.addCell("Telefono");
			tablachofer2.addCell("Correo Electronico.");
			tablachofer2.addCell("ID Estado");
			tablachofer2.addCell("ID Municipio");
			tablachofer3.addCell("ID Vehiculo");
			
			//Colocamos el nombre a cada columna de la tabla estado
			tablaestado.addCell("C�digo.");
			tablaestado.addCell("Estado.");
			
			//Colocamos el nombre a cada columna de la tabla municipio
			tablamunicipio.addCell("C�digo");
			tablamunicipio.addCell("Municipio");
			
			//Colocamos el nombre para las columnas de la tabla vehiculo
			tablavehiculo.addCell("C�digo");
			tablavehiculo.addCell("Placa del Vehiculo.");
			tablavehiculo.addCell("Modelo del Vehiculo.");
			tablavehiculo.addCell("Marca del Vehiculo.");
			
			//Creamos un parrafo vacio para que haya un espacio entre las tablas
			Paragraph vacio = new Paragraph();
		    vacio.add("\n");
		    
		    //Linea separadora
		    Paragraph Separador = new Paragraph();
		    Separador.add("��������������������������������������");
		    
		    //Identificadores para las tablas
			Paragraph TChofer =new Paragraph("TABLA CHOFER",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));
			Paragraph TVehiculo =new Paragraph("TABLA VEHICULO",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));
			Paragraph TEstado =new Paragraph("TABLA ESTADO",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));
			Paragraph TMunicipio =new Paragraph("TABLA MUNICIPIO",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));

			//Conectar con la base de datos
			try {
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
				stt = conexion.createStatement();
				
				//Usar una base de datos
				
				String use = "use urbantaxi;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
				//Antes de todo se a�ade el titulo
			    documento.add(TChofer); documento.add(vacio);
			    
				//----------------------->TABLA DE CHOFER<---------------------------
			
				 	PSChofer = conexion.prepareStatement("Select * from chofer;");
					
					ResultSet rs = PSChofer.executeQuery();
					
					if(rs.next()) { //Si hay datos en la base de datos
						
						do {
							//Se coloca para a�adir los valores de la base de datos en la tabla de pdf
							tablachofer1.addCell(rs.getString(1)); //Se a�ade el texto a la primera columna
							tablachofer1.addCell(rs.getString(2)); //Se a�ade el texto a la segunda columna
							tablachofer1.addCell(rs.getString(3));
							tablachofer1.addCell(rs.getString(4));
							tablachofer2.addCell(rs.getString(5));
							tablachofer2.addCell(rs.getString(6));
							tablachofer2.addCell(rs.getString(7));
							tablachofer2.addCell(rs.getString(8));
							tablachofer3.addCell(rs.getString(9));	
																			
						}while(rs.next());

						//Se a�ade todo al documento
						documento.add(tablachofer1); documento.add(vacio);
						documento.add(tablachofer2); documento.add(vacio);
						documento.add(tablachofer3); documento.add(vacio);
						documento.add(vacio); documento.add(Separador); documento.add(vacio);
					}
				
				//----------------------->TABLA DE VEHICULO<---------------------------
				documento.add(TVehiculo); documento.add(vacio);

				PSVehiculo = conexion.prepareStatement("Select * from vehiculo");
				
				rs = PSVehiculo.executeQuery();
				
				if(rs.next()) { //Si hay datos en la base de datos
					
					do {
						//Se coloca para a�adir los valores de la base de datos en la tabla de pdf
						tablavehiculo.addCell(rs.getString(1)); //Se a�ade el texto a la primera columna
						tablavehiculo.addCell(rs.getString(2)); //Se a�ade el texto a la segunda columna
						tablavehiculo.addCell(rs.getString(3)); //Se a�ade el texto a la tercera columna y as�
						tablavehiculo.addCell(rs.getString(4));
					}while(rs.next());
					
					//Se a�ade todo al documento
					documento.add(tablavehiculo);
					documento.add(vacio); documento.add(Separador); documento.add(vacio);
					
					//----------------------->TABLA DE ESTADO<---------------------------

				    documento.add(TEstado); documento.add(vacio);

					PSEstado = conexion.prepareStatement("Select * from estado;");
					
					rs = PSEstado.executeQuery();
					
					if(rs.next()) { //Si hay datos en la base de datos
						
						do {
							//Se coloca para a�adir los valores de la base de datos en la tabla de pdf
							tablaestado.addCell(rs.getString(1)); //Se a�ade el texto a la primera columna
							tablaestado.addCell(rs.getString(2)); //Se a�ade el texto a la segunda columna
						}while(rs.next());
						
						//Se a�ade todo al documento
						documento.add(tablaestado);
						documento.add(vacio); documento.add(Separador); documento.add(vacio);
					}
					
					//----------------------->TABLA DE MUNICIPIO<---------------------------
					
				    documento.add(TMunicipio); documento.add(vacio);

					PSMunicipio = conexion.prepareStatement("Select * from municipio;");
					
					rs = PSMunicipio.executeQuery();
					
					if(rs.next()) { //Si hay datos en la base de datos
						
						do {
							//Se coloca para a�adir los valores de la base de datos en la tabla de pdf
							tablamunicipio.addCell(rs.getString(1)); //Se a�ade el texto a la primera columna
							tablamunicipio.addCell(rs.getString(2)); //Se a�ade el texto a la segunda columna
						}while(rs.next());
						
						//Se a�ade todo al documento
						documento.add(tablamunicipio);
						documento.add(vacio); documento.add(Separador); documento.add(vacio);
					}
				}
			}catch(DocumentException | SQLException e) {}
			//Cerramos el documento
			documento.close();
			JOptionPane.showMessageDialog(null,"Reporte creado con �xito\n en el escritorio."); //Mensaje de verificacion que ya se hizo el pdf
		}catch(DocumentException | HeadlessException | FileNotFoundException a) {
			
		}
		
		
	}

	//Metodo para devolver el id del vehiculo
	public int IDVE(Mod Mode) {
		
		//Variables necesarias para buscar el id del vehiculo
		String placa = Mode.getplacaVE();String modelo = Mode.getmodeloVE();String marca = Mode.getmarcaVE();
		try {
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			Mistatement = conexion.prepareStatement("Select ID_Vehiculo from vehiculo where Placa_Vehiculo like ? and Modelo_Vehiculo like ? and Marca_Vehiculo like ?;");
			Mistatement.setString(1,placa);
			Mistatement.setString(2,modelo);
			Mistatement.setString(3, marca);
			
			ResultSet rs = Mistatement.executeQuery();
			if(rs.next()) { //Si hay datos en la base de datos
				do {
					idvehiculo = rs.getInt("ID_Vehiculo"); 
				}while(rs.next());
			}
		}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del vehiculo"); e.printStackTrace();}
		return idvehiculo;
	}
		
	public int IDestado(Mod Mode) {
		
		//Variables necesarias para buscar el id del vehiculo
		int ci = Mode.getCi();
		try {
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			Mistatement = conexion.prepareStatement("Select ID_Estado from chofer where cedula =?");
			Mistatement.setInt(1,ci);
			ResultSet rs = Mistatement.executeQuery();
			if(rs.next()) { //Si hay datos en la base de datos
				do {
					idestado = rs.getInt("ID_Estado"); 
				}while(rs.next());
			}
		}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del vehiculo"); e.printStackTrace();}
		return idestado;
	}

	public int IDvehiculo(Mod Mode) {
		
		//Variables necesarias para buscar el id del vehiculo
		int ci = Mode.getCi();
		try {			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use

			Mistatement = conexion.prepareStatement("Select ID_Vehiculo from chofer WHERE cedula = ?;");
			Mistatement.setInt(1,ci);
			ResultSet rs = Mistatement.executeQuery();
			if(rs.next()) { //Si hay datos en la base de datos
				do {
					 idve = rs.getInt("ID_Vehiculo"); 
				}while(rs.next());
			}
		}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del vehiculo"); e.printStackTrace();}
		return idve;
	}
	
	public int IDmunicipio(Mod Mode) {
		
		//Variables necesarias para buscar el id del vehiculo
		int ci = Mode.getCi();
		try {
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
			//2. Creacion del Statement
			
			stt = conexion.createStatement();
			
			//Usar una base de datos
			
			String use = "use urbantaxi;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			Mistatement = conexion.prepareStatement("Select ID_Municipio from chofer where cedula = ?");
			Mistatement.setInt(1,ci);
			ResultSet rs = Mistatement.executeQuery();
			if(rs.next()) { //Si hay datos en la base de datos
				do {
					idmunicipio = rs.getInt("ID_Municipio"); 
				}while(rs.next());
			}
		}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del vehiculo"); e.printStackTrace();}
		return idmunicipio;
	}
}
