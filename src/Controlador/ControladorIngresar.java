package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import Modelo.Mod;
import Principal.Conexion;
import Vista.VistaIngresar;

public class ControladorIngresar implements ActionListener, KeyListener {

	//Componentes
	VistaIngresar Vingresar;
	Conexion con = new Conexion();
	Mod mode;
	
	public ControladorIngresar(VistaIngresar Vin) {
	
		//La declaramos aqui
		 Vingresar = Vin;
		 mode = new Mod();
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		//Si se presiona enviar
		if(event.getSource().equals(Vingresar.Enviar)) {
			
			//Si alguno de los campos esta vacio
			
			if(Vingresar.NombreC.getText().isEmpty() || Vingresar.Cedula.getText().isEmpty() 
				|| Vingresar.Direccion.getText().isEmpty() || Vingresar.Tlf.getText().isEmpty() || Vingresar.CorreoE.getText().isEmpty() 
				|| Vingresar.Estado.getText().isEmpty() || Vingresar.Municipio.getText().isEmpty() || Vingresar.PlacaVe.getText().isEmpty() 
				|| Vingresar.ModeloVe.getText().isEmpty() || Vingresar.MarcaVe.getText().isEmpty()) { //Si tocan el boton enviar

			JOptionPane.showMessageDialog(null,"Alguno de los campos est� vac�o.\nDebe llenar todos los campos.");
			System.out.println("Alguno de los campos esta vacio");
			
			} else {
				//Enviar todos los datos al modulo
				System.out.println("Campos llenos y listos para ser enviados a la base de datos "+Vingresar.NombreC.getText());
							
				//Inicializar los datos
				
				//Se establece los datos ingresados
				mode.setNombreCompleto(Vingresar.NombreC.getText());
				mode.setCi(Vingresar.Cedula.getText());
				mode.setDir(Vingresar.Direccion.getText());
				mode.setTLF(Vingresar.Tlf.getText());
				mode.setCorreoElec(Vingresar.CorreoE.getText());
				//mode.setestado(Vingresar.Estado.getText());
				//mode.setmunicipio(Vingresar.Municipio.getText());
				mode.setplacaVE(Vingresar.PlacaVe.getText());
				mode.setmodeloVE(Vingresar.ModeloVe.getText());
				mode.setmarcaVE(Vingresar.MarcaVe.getText());
				
				//Se mandan todos los datos a la base de datos
				//con.Insercion();
			}
		}

		
		if(event.getSource().equals(Vingresar.Regresar)) {
			
		}
		
	}
	
	public void Limpiar() {
		//Limpia todos los JTextField
		Vingresar.NombreC.setText("");
		Vingresar.Cedula.setText(""); 
		Vingresar.Direccion.setText("");
		Vingresar.Tlf.setText("");
		Vingresar.CorreoE.setText("");
		Vingresar.Estado.setText("");
		Vingresar.Municipio.setText("");
		Vingresar.PlacaVe.setText(""); 
		Vingresar.ModeloVe.setText("");
		Vingresar.MarcaVe.setText("");
	}

	public void keyTyped(KeyEvent e) {
		
		//Poniendo el limite a los inputs
		if(Vingresar.NombreC.getText().length() == 200) { e.consume();}
		
		if(Vingresar.Direccion.getText().length() == 200) {e.consume();}
		
		if(Vingresar.Tlf.getText().length() == 50) {e.consume();}
		
		if(Vingresar.CorreoE.getText().length() == 100) {e.consume();}
		
		if(Vingresar.Estado.getText().length() == 50) {	e.consume();}
		
		if(Vingresar.Municipio.getText().length() == 50) {e.consume();}
		
		if(Vingresar.PlacaVe.getText().length() == 12) {e.consume();}
		
		if(Vingresar.ModeloVe.getText().length() == 50) {e.consume();}
		
		if(Vingresar.MarcaVe.getText().length() == 50) {e.consume();}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
		//Volver mayuscula las letras ingresadas
		Vingresar.PlacaVe.setText(Vingresar.PlacaVe.getText().toUpperCase());
	}

}
