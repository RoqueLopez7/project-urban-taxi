package Controlador;

//import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import Modelo.Mod;
import Principal.Conexion;
//Primero el tipo y luego el nombre de la clase 
import Vista.Vista; //Importamos la clase vista
//import Vista.VistaIngresar;
//import Vista.VistaMenu;

public class ControladorPrincipal implements ActionListener, KeyListener{

	//Atributos
	public Vista Ventana;
	public Conexion con = new Conexion();
	public Mod mode;
	
	public int cedula, cedulaAct;
	public boolean Encontrado;
	
	public ControladorPrincipal(Vista ventana) {
 
		//La declaramos aqui
		Ventana = ventana;
		mode = new Mod();
	}

	@Override
	public void actionPerformed(ActionEvent Evento) {
		
	/*----------------------------------------------------------------------------
	 *  Acciones para el segmento MENU
	 * ----------------------------------------------------------------------------
	 */	
		
		//Si presionan ingresar
		if(Evento.getSource().equals(Ventana.Ingresar)) { 
			//Visible el panel de ingresar y desactiva el menu
			Ventana.Panel2.setVisible(true);
			Ventana.Panel1.setVisible(false);
		}
		
		
		//Si presionan buscar
		if(Evento.getSource().equals(Ventana.Buscar)) { 
			//Visible el panel de Buscar y desactiva el menu
			Ventana.Panel3.setVisible(true);
			Ventana.Panel1.setVisible(false);
		}
		
		//Si presionan Actualizar
		if(Evento.getSource().equals(Ventana.Actualizar)) { 
			//Visible el panel de Actualizar y desactiva el menu
			Ventana.Panel4.setVisible(true);
			Ventana.Panel1.setVisible(false);
		}
		
		//Si presionan Eliminar
		if(Evento.getSource().equals(Ventana.Eliminar)) { 
			//Visible el panel de Eliminar y desactiva el menu
			Ventana.Panel5.setVisible(true);
			Ventana.Panel1.setVisible(false);
		}
		
		//Si presionan salir
		if(Evento.getSource().equals(Ventana.Salir)) { 
			System.exit(0);
		}
		
		//Si presionan generar reporte
		if(Evento.getSource().equals(Ventana.Reporte)) {
			con.Reporte();
		}
		
	/*----------------------------------------------------------------------------
	 *  ACCIONES PARA LOS BOTONES REGRESAR
	 * ----------------------------------------------------------------------------
	 */		
		
		//Si presionan regresar del segmento Ingresar
		if(Evento.getSource().equals(Ventana.Regresar)) {
			//Regresando del segmento ingresar al menu
			Ventana.Panel2.setVisible(false);
			Ventana.Panel1.setVisible(true);
			Limpiar();
		}
		
		//Si presionan regresar del segmento Buscar
		if(Evento.getSource().equals(Ventana.Regresar2)) {
			//Regresando del segmento buscar al menu
			Ventana.Panel3.setVisible(false);
			Ventana.Panel1.setVisible(true);
			Ventana.InputBus.setText("");
			Limpiar();
		}
		
		//Si presionan regresar del segmento Actualizar
		if(Evento.getSource().equals(Ventana.Regresar3)) {
			//Regresando del segmento Actualizar al menu
			Ventana.Panel4.setVisible(false);
			Ventana.Panel1.setVisible(true);
			Ventana.InputAct.setText("");
			Limpiar();
		}
		
		//Si presionan regresar del segmento Eliminar
		if(Evento.getSource().equals(Ventana.Regresar4)) {
			//Regresando del segmento Eliminar al menu
			Ventana.Panel5.setVisible(false);
			Ventana.Panel1.setVisible(true);
			Ventana.InputBor.setText("");
			Limpiar();
		}
		
	/*----------------------------------------------------------------------------
	 *  Acciones para el segmento INGRESAR
	 * ----------------------------------------------------------------------------
	 */
			if(Evento.getSource().equals(Ventana.Enviar)) {

				//Si alguno de los campos esta vacio y Si estan en el segmento ingresar
				if(Ventana.NombreC.getText().isEmpty() || Ventana.Cedula.getText().isEmpty() 
					|| Ventana.Direccion.getText().isEmpty() || Ventana.Tlf.getText().isEmpty() || Ventana.CorreoE.getText().isEmpty() 
					|| Ventana.Estado.getSelectedItem().equals("") || Ventana.Municipio.getSelectedItem().equals("") || Ventana.PlacaVe.getText().isEmpty() 
					|| Ventana.ModeloVe.getText().isEmpty() || Ventana.MarcaVe.getText().isEmpty()) { //Si tocan el boton enviar
		
					//Informaci�n sobre que alguno de los campos est� vacio
					JOptionPane.showMessageDialog(null,"Alguno de los campos est� vac�o.\nDebe llenar todos los campos.");						
					
					} else {
					//Llamamos al metodo para que envie los datos al modulo	
					if(Ventana.Estado.getSelectedIndex() != 0) {
						EnviarModulo();
						
						//Se mandan todos los datos a la base de datos
						con.Insercion(mode);
						Limpiar(); //Limpia todos los campos
					} else { 
						//Mensaje informativo para asegurar el Estado
						JOptionPane.showMessageDialog(null, "Algo anda mal en el Estado seleccionado, verifique que es el correcto");
					}
				}		
			}		
			
	/*----------------------------------------------------------------------------
	 *  Acciones para el segmento BUSCAR	
	 * ----------------------------------------------------------------------------
	 */
		//Si presionan el boton buscar
		if(Evento.getSource().equals(Ventana.BuscarConductor)) {
			
			//----------->Si el campo de texto de buscar est� vacio<-----------
			if(Ventana.InputBus.getText().isEmpty() && Ventana.Panel3.isVisible()) {
				
				//Salto de mensaje de advertencia
				JOptionPane.showMessageDialog(null,"El campo est� vac�o.\nDebe llenar el campo.");
			
			} else {
				if(Ventana.Panel3.isVisible()) {
					//Busqueda de un conductor
					//Manda la cedula al modulo
					mode.setCi(Ventana.InputBus.getText());

					//Llama el metodo busqueda de conexion y mensaje de esta misma clase y establece un valor al area de texto
					con.Busqueda(mode);
					Mensaje();
					Ventana.Area.setText(con.Datos);
				}
			}
		}
		
		//----------->Si hacen una consulta general<-----------
		if(Evento.getSource().equals(Ventana.ConsultaGeneral)) {
			
			if(con.Datos != null) {
				con.Datos = "";
			}
			con.ConsultaG(mode);
			//Ventana.Area.setText("");
			Ventana.Area.setText(con.Datos+"Fin de la n�mina.\n-----------------------------------------------------------------------------");
		}
		
	/*----------------------------------------------------------------------------
	 *  Acciones para el segmento ACTUALIZAR	
	 * ----------------------------------------------------------------------------
	 */
		
		//----------->Si buscan a la persona por la cedula<-----------
		if(Evento.getSource().equals(Ventana.BuscarAct)) {
			
			//----------->Si el campo de actualizar esta vacio<-----------
			if(Ventana.InputAct.getText().isEmpty() && Ventana.Panel4.isVisible()) {
				
				//Salto de mensaje de advertencia
				JOptionPane.showMessageDialog(null,"El campo est� vac�o.\nDebe llenar el campo.");
			} else {
				if(Ventana.Panel4.isVisible()) {
					//Busqueda de un conductor
					//Manda la cedula al modulo
					mode.setCi(Ventana.InputAct.getText());
					
					//Llama el metodo busqueda de conexion y mensaje de esta misma clase y establece un valor al area de texto
					con.Busqueda(mode);
					Mensaje();
					Ventana.AreaAct.setText(con.Datos);
				}
			}
		}
		
		if(Evento.getSource().equals(Ventana.ActualizarAc)) {
			
			//Si el cliente esta en el segmento Actualizar y alguno de los campos est� vacio
			if(Ventana.NombreCAc.getText().isEmpty() || Ventana.CedulaAc.getText().isEmpty() 
				|| Ventana.DireccionAc.getText().isEmpty() || Ventana.TlfAc.getText().isEmpty() || Ventana.CorreoEAc.getText().isEmpty() 
				|| Ventana.ListaEstadoAc.getSelectedItem().equals("") || Ventana.ListaMunicipioAc.getSelectedItem().equals("") || Ventana.PlacaVeAc.getText().isEmpty() 
				|| Ventana.ModeloVeAc.getText().isEmpty() || Ventana.MarcaVeAc.getText().isEmpty()) { //Si tocan el boton enviar
	
					//Informaci�n sobre que alguno de los campos est� vacio
					JOptionPane.showMessageDialog(null,"Alguno de los campos est� vac�o.\nDebe llenar todos los campos.");						
					
				} else {
						
					//Llamamos al metodo para que envie los datos al modulo	
					EnviarModulo();
							
					//----------->Seguridad para que no cambie los datos del conductor EN ACTUALIZAR<-----------
					cedula = Integer.parseInt(Ventana.InputAct.getText());
						
					if(cedula != cedulaAct && Ventana.Panel4.isVisible()) {
			
						//Mesaje de que se ha cambiado la cedula
						JOptionPane.showMessageDialog(null, "La cedula se ha modificado.\nVerifique la cedula ingresada al principio.");
						
					} else if(Ventana.Panel4.isVisible()){
					
						//Se actualiza los datos de un conductor
						mode.setci2(Ventana.InputAct.getText());
						con.Actualizacion(mode);
					}
				//Limpia los campos
				Limpiar();
			}
		}
		
	/*----------------------------------------------------------------------------
	 *  Acciones para el segmento BORRAR	
	 * ----------------------------------------------------------------------------
	 */
		//----------->Si Buscan a una persona por su cedula en BORRAR<-----------
		if(Evento.getSource().equals(Ventana.BuscarBor)) {
			
			//----------->Si el campo de borrar esta vacio<-----------
			if(Ventana.InputBor.getText().isEmpty() && Ventana.Panel5.isVisible()) {
				
				//Salto de mensaje de advertencia
				JOptionPane.showMessageDialog(null,"El campo est� vac�o.\nDebe llenar el campo.");
			} else {
				if(Ventana.Panel5.isVisible()) {
					//Busqueda de un conductor
					//Manda la cedula al modulo
					mode.setCi(Ventana.InputBor.getText());
					cedula = Integer.parseInt(Ventana.InputBor.getText());
					
					//Llama el metodo busqueda de conexion y mensaje de esta misma clase y establece un valor al area de texto
					con.Busqueda(mode);
					Mensaje();
					Ventana.AreaBor.setText(con.Datos);
					
					if(Encontrado) {
						//Desbloquear los botones y volver visible la pregunta solo si se ha encontrado la persona
						Ventana.Afirmar.setEnabled(true);
						Ventana.Afirmar.setVisible(true);
						Ventana.Negar.setEnabled(true);
						Ventana.Negar.setVisible(true);
						Ventana.DeseaBor.setVisible(true);
					} else if(Encontrado == false) {
						//Bloquea los botones y vuelve invisible la pregunta
						Ventana.Afirmar.setEnabled(false);
						Ventana.Afirmar.setVisible(false);
						Ventana.Negar.setEnabled(false);
						Ventana.Negar.setVisible(false);
						Ventana.DeseaBor.setVisible(false);
					}
				}
			}
		}
		
		//----------->Si presionan el boton si o no<-----------
		if(Evento.getSource().equals(Ventana.Afirmar) || Evento.getSource().equals(Ventana.Negar)) {
			
			//----------->Seguridad para que no cambie los datos del conductor EN BORRAR<-----------
				
			if(cedula != con.cedulaBor) {
	
				//Mesaje de que se ha cambiado la cedula
				JOptionPane.showMessageDialog(null, "La cedula se ha modificado.\nVerifique la cedula ingresada al principio.");
				
			} else {
				//Si afirma la eliminacion del conductor
				if(Evento.getSource().equals(Ventana.Afirmar)) {
					
					//Se borra el conductor
					mode.setCi(Ventana.InputBor.getText());
					con.Borrar(mode);
					
					//Limpieza de los inputs y desactivacion de los botones
 					Limpiar();
 					mode.Limpiar();
					
				} 
			}
			
			//Si niega la eliminacion del conductor
			if(Evento.getSource().equals(Ventana.Negar)){
				//No se borra el conductor
				Limpiar();
			}
		}
		
		if(Evento.getSource().equals(Ventana.Estado)) {
			CambioMunicipio();
		}
		
		if(Evento.getSource().equals(Ventana.ListaEstadoAc)) {CambioMunicipio();}
	}
	
	public void Limpiar() {
		//Limpia todos los JTextField y JTextArea
		//Ingresar
		Ventana.NombreC.setText("");
		Ventana.Cedula.setText(""); 
		Ventana.Direccion.setText("");
		Ventana.Tlf.setText("");
		Ventana.CorreoE.setText("");
		Ventana.Estado.setSelectedIndex(0);
		CambioMunicipio();
		Ventana.Municipio.removeAllItems(); Ventana.Municipio.setEnabled(false);
		Ventana.PlacaVe.setText(""); 
		Ventana.ModeloVe.setText("");
		Ventana.MarcaVe.setText("");
		//Buscar
		Ventana.InputBus.setText("");
		Ventana.Area.setText("");
		Ventana.AreaAct.setText("");
		//Actualizar
		Ventana.NombreCAc.setText("");
		Ventana.CedulaAc.setText(""); 
		Ventana.DireccionAc.setText("");
		Ventana.TlfAc.setText("");
		Ventana.CorreoEAc.setText("");
		Ventana.ListaEstadoAc.setSelectedIndex(0);
		Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.setEnabled(false);
		Ventana.PlacaVeAc.setText(""); 
		Ventana.ModeloVeAc.setText("");
		Ventana.MarcaVeAc.setText("");
		//Borrar
		Ventana.InputBor.setText("");
		Ventana.AreaBor.setText("");
		Ventana.Afirmar.setEnabled(false);
		Ventana.Negar.setEnabled(false);
		Ventana.DeseaBor.setVisible(false);
		Ventana.Afirmar.setVisible(false);
		Ventana.Negar.setVisible(false);
		Ventana.DeseaBor.setVisible(false);
	}
	
	public void EnviarModulo() {
		//Enviar todos los datos al modulo
		if(Ventana.Panel2.isVisible()) {
			//Se establece los datos ingresados
			mode.setNombreCompleto(Ventana.NombreC.getText());
			mode.setCi(Ventana.Cedula.getText());
			mode.setDir(Ventana.Direccion.getText());
			mode.setTLF(Ventana.Tlf.getText());
			mode.setCorreoElec(Ventana.CorreoE.getText());
			EnviarModuloEstadoMunicipio();
			mode.setplacaVE(Ventana.PlacaVe.getText());
			mode.setmodeloVE(Ventana.ModeloVe.getText());
			mode.setmarcaVE(Ventana.MarcaVe.getText());
		} else if (Ventana.Panel4.isVisible()) {
			//Se establece los datos ingresados
			mode.setNombreCompleto(Ventana.NombreCAc.getText());
			mode.setCi(Ventana.CedulaAc.getText());
			mode.setDir(Ventana.DireccionAc.getText());
			mode.setTLF(Ventana.TlfAc.getText());
			mode.setCorreoElec(Ventana.CorreoEAc.getText());
			EnviarModuloEstadoMunicipio();
			mode.setplacaVE(Ventana.PlacaVeAc.getText());
			mode.setmodeloVE(Ventana.ModeloVeAc.getText());
			mode.setmarcaVE(Ventana.MarcaVeAc.getText());
			//Asignamos el valor de la cedula para la seguridad del programa
			cedulaAct = mode.getCi();
		}
	}

	public void Mensaje() {
		
		//------------>Mensaje para informar si se encontro o no el conductor<------------
		int CiBus = mode.getCi();
		int CiAct = mode.getCi();
		int CiBor = mode.getCi();
		
		//Condicionales si la cedula de la tabla coincide con la ingresada y si esta en el panel correspondiente al segmento
		if(con.cedula == CiBus && Ventana.Panel3.isVisible()) { JOptionPane.showMessageDialog(null,"Conductor encontrado."); CiBus = 0;} 
		else { if(Ventana.Panel3.isVisible() && con.cedula != CiBus) {JOptionPane.showMessageDialog(null, "Conductor NO encontrado."); CiBus = 0; Ventana.Area.setText(""); con.Datos = null;}}
		
		if(con.cedula == CiAct && Ventana.Panel4.isVisible()) { JOptionPane.showMessageDialog(null,"Conductor encontrado."); CiAct = 0;} 
		else { if(Ventana.Panel4.isVisible() && con.cedula != CiAct) {JOptionPane.showMessageDialog(null, "Conductor NO encontrado."); CiAct = 0; Ventana.AreaAct.setText(""); con.Datos = null;}}
		
		if(con.cedula == CiBor && Ventana.Panel5.isVisible()) { JOptionPane.showMessageDialog(null,"Conductor encontrado."); CiBor = 0; Encontrado = true;} 
		else { if(Ventana.Panel5.isVisible() && con.cedula != CiBor) {JOptionPane.showMessageDialog(null,"No se ha encontrado el conductor.\nVerifique la cedula del mismo.");; CiBor = 0; Ventana.AreaBor.setText(""); con.Datos = null; Encontrado = false;}}
		
		//Reseteo de la variable
		con.cedula = 0;
	}
	
	public void keyTyped(KeyEvent e) {
		
		//Poniendo el limite a los inputs
		if(Ventana.NombreC.getText().length() == 200) { e.consume();}
		
		if(Ventana.Direccion.getText().length() == 200) {e.consume();}
		
		if(Ventana.Tlf.getText().length() == 50) {e.consume();}
		
		if(Ventana.CorreoE.getText().length() == 100) {e.consume();}

		if(Ventana.PlacaVe.getText().length() == 12) {e.consume();}
		
		if(Ventana.ModeloVe.getText().length() == 50) {e.consume();}
		
		if(Ventana.MarcaVe.getText().length() == 50) {e.consume();}
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
		//Volver mayuscula las letras ingresadas
		Ventana.PlacaVe.setText(Ventana.PlacaVe.getText().toUpperCase());
		Ventana.PlacaVeAc.setText(Ventana.PlacaVeAc.getText().toUpperCase());
	}

	public void CambioMunicipio() {
		
		if(Ventana.Panel2.isVisible()) {
			if(Ventana.Estado.getSelectedIndex() == 0)
			{
				Ventana.Municipio.removeAllItems(); 
				Ventana.Municipio.setEnabled(false);
			}
			else
			{	
				Ventana.Municipio.setEnabled(true);
				switch(Ventana.Estado.getSelectedIndex()) {
	/*D.C*/  	    case 1: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Libertador"); break;
	/*Amazonas*/	case 2: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Alto Orinoco"); Ventana.Municipio.addItem("Atabapo"); Ventana.Municipio.addItem("Altures"); Ventana.Municipio.addItem("Autana"); Ventana.Municipio.addItem("Manapiare"); Ventana.Municipio.addItem("Maroa"); Ventana.Municipio.addItem("R�o Negro"); break;
	/*Anzo�tegui*/	case 3: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Anaco"); Ventana.Municipio.addItem("Aragua"); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Bruzual"); Ventana.Municipio.addItem("Cajigal"); Ventana.Municipio.addItem("Carvajal"); Ventana.Municipio.addItem("Diego Bautista Urbaneja"); Ventana.Municipio.addItem("Freites"); Ventana.Municipio.addItem("Guanipa"); Ventana.Municipio.addItem("Guanta"); Ventana.Municipio.addItem("Independencia"); Ventana.Municipio.addItem("Libertad"); Ventana.Municipio.addItem("McGregor"); Ventana.Municipio.addItem("Miranda"); Ventana.Municipio.addItem("Monagas"); Ventana.Municipio.addItem("Pe�alver"); Ventana.Municipio.addItem("P�ritu"); Ventana.Municipio.addItem("San Juan de Capistrano"); Ventana.Municipio.addItem("Santa Ana"); Ventana.Municipio.addItem("Sim�n Rodriguez"); Ventana.Municipio.addItem("Sotillo"); break;
	/*Apure*/		case 4: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Achaguas"); Ventana.Municipio.addItem("Biruaca"); Ventana.Municipio.addItem("Mu�oz"); Ventana.Municipio.addItem("Pa�z"); Ventana.Municipio.addItem("Pedro Camejo"); Ventana.Municipio.addItem("R�mulo Gallegos"); Ventana.Municipio.addItem("San Fernando"); break;
	/*Aragua*/		case 5: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Camatagua"); Ventana.Municipio.addItem("Francisco Linares Alc�ntara"); Ventana.Municipio.addItem("Girardot"); Ventana.Municipio.addItem("Jos� �ngel Lamas"); Ventana.Municipio.addItem("Jos� F�lix Ribas"); Ventana.Municipio.addItem("Jos� Rafael Revenga"); Ventana.Municipio.addItem("Libertador"); Ventana.Municipio.addItem("Mario Brice�o Iragorry"); Ventana.Municipio.addItem("Ocumare de la Costa de Oro"); Ventana.Municipio.addItem("San Casimiro"); Ventana.Municipio.addItem("San Sebasti�n"); Ventana.Municipio.addItem("Santiago Mari�o"); Ventana.Municipio.addItem("Santos Michelena"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Tovar"); Ventana.Municipio.addItem("Urdaneta"); Ventana.Municipio.addItem("Zamora"); break;
	/*Barinas*/		case 6: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Alberto Arvelo Torrealba"); Ventana.Municipio.addItem("Andres Eloy Blanco"); Ventana.Municipio.addItem("Antonio Jos� de Sucre"); Ventana.Municipio.addItem("Arismendi"); Ventana.Municipio.addItem("Barinas"); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Cruz Paredes"); Ventana.Municipio.addItem("Ezequiel Zamora"); Ventana.Municipio.addItem("Obispos"); Ventana.Municipio.addItem("Pedraza"); Ventana.Municipio.addItem("Rojas"); Ventana.Municipio.addItem("Sosa"); break;
	/*Bol�var*/		case 7: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Caron�"); Ventana.Municipio.addItem("Cede�o"); Ventana.Municipio.addItem("El Callao"); Ventana.Municipio.addItem("Gran Sabana"); Ventana.Municipio.addItem("Heres"); Ventana.Municipio.addItem("Piar"); Ventana.Municipio.addItem("Ra�l Leoni"); Ventana.Municipio.addItem("Roscio"); Ventana.Municipio.addItem("Sifontes"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Padre Pedro Chen");break;
	/*Carabobo*/	case 8: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Bejuma"); Ventana.Municipio.addItem("Carlos Arvelo"); Ventana.Municipio.addItem("Diego Ibarra"); Ventana.Municipio.addItem("Guacara"); Ventana.Municipio.addItem("Juan Jos� Mora"); Ventana.Municipio.addItem("Libertador"); Ventana.Municipio.addItem("Los Guayos"); Ventana.Municipio.addItem("Miranda"); Ventana.Municipio.addItem("Montalb�n"); Ventana.Municipio.addItem("Naguanagua"); Ventana.Municipio.addItem("Puerto Cabello"); Ventana.Municipio.addItem("San Diego"); Ventana.Municipio.addItem("San Joaquin"); Ventana.Municipio.addItem("Valencia"); break;
	/*Cojedes*/		case 9: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Anzo�tegui"); Ventana.Municipio.addItem("Falc�n"); Ventana.Municipio.addItem("Girardot"); Ventana.Municipio.addItem("Lima Blanco"); break;
	/*DeltaAmacuro*/case 10: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Antonio D�az Curiapo"); Ventana.Municipio.addItem("Casacoima"); Ventana.Municipio.addItem("Perdernales"); Ventana.Municipio.addItem("Tucupita"); break;
	/*Falcon*/		case 11: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Acosta"); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Buchivacoa"); Ventana.Municipio.addItem("Cacique Manaure"); Ventana.Municipio.addItem("Carirubana"); Ventana.Municipio.addItem("Colina"); Ventana.Municipio.addItem("Dabajuro"); Ventana.Municipio.addItem("Democracia"); Ventana.Municipio.addItem("Falc�n"); Ventana.Municipio.addItem("Federaci�n"); Ventana.Municipio.addItem("Jacura"); Ventana.Municipio.addItem("Los Taques"); Ventana.Municipio.addItem("Mauroa"); Ventana.Municipio.addItem("Miranda"); Ventana.Municipio.addItem("Monse�or Iturriza"); Ventana.Municipio.addItem("Palmasola"); Ventana.Municipio.addItem("Petit"); Ventana.Municipio.addItem("P�ritu"); Ventana.Municipio.addItem("San Francisco"); Ventana.Municipio.addItem("Silva"); Ventana.Municipio.addItem("Toc�pero"); Ventana.Municipio.addItem("Uni�n"); Ventana.Municipio.addItem("Urumaco"); Ventana.Municipio.addItem("Zamora"); break;
	/*Guarico*/		case 12: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Esteros de Camaguan"); Ventana.Municipio.addItem("Chaguaramas"); Ventana.Municipio.addItem("El Socorro"); Ventana.Municipio.addItem("Francisco de Miranda"); Ventana.Municipio.addItem("Jos� F�lix Ribas"); Ventana.Municipio.addItem("Jos� Tadeo Monagas"); Ventana.Municipio.addItem("Juan Germ�n Rosci�"); Ventana.Municipio.addItem("Juli�n Mellado"); Ventana.Municipio.addItem("Las Mercedes"); Ventana.Municipio.addItem("Leonardo Infante"); Ventana.Municipio.addItem("Pedro Zaraza"); Ventana.Municipio.addItem("Ortiz"); Ventana.Municipio.addItem("San Ger�nimo del Guayabal"); Ventana.Municipio.addItem("San Jos� de Guaribe"); Ventana.Municipio.addItem("Santa Mar�a de Ipire"); break;
	/*Lara*/		case 13: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Andr�s Eloy Blanco"); Ventana.Municipio.addItem("Crespo"); Ventana.Municipio.addItem("Iribarren"); Ventana.Municipio.addItem("Jim�nez"); Ventana.Municipio.addItem("Mor�n"); Ventana.Municipio.addItem("Palavecino"); Ventana.Municipio.addItem("Sim�n Planas"); Ventana.Municipio.addItem("Torres"); Ventana.Municipio.addItem("Urdaneta"); break;
	/*Merida*/		case 14: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Alberto Adriani"); Ventana.Municipio.addItem("Andr�s Bello"); Ventana.Municipio.addItem("Antonio Pinto Salinas"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Tovar"); Ventana.Municipio.addItem("Tulio Febres Cordero"); Ventana.Municipio.addItem("Zea"); break;
	/*Miranda*/		case 15: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Acevedo"); Ventana.Municipio.addItem("Andr�s Bello"); Ventana.Municipio.addItem("Baruto"); Ventana.Municipio.addItem("Bri�n"); Ventana.Municipio.addItem("Buroz"); Ventana.Municipio.addItem("Carrizal"); Ventana.Municipio.addItem("Chacao"); Ventana.Municipio.addItem("Cristobal Rojas"); Ventana.Municipio.addItem("El Hatillo"); Ventana.Municipio.addItem("Gaicaipuro"); Ventana.Municipio.addItem("Independencia"); Ventana.Municipio.addItem("Lander"); Ventana.Municipio.addItem("Los Salias"); Ventana.Municipio.addItem("P�ez"); Ventana.Municipio.addItem("Paz Castillo");  Ventana.Municipio.addItem("Pedro Gual"); Ventana.Municipio.addItem("Plaza"); Ventana.Municipio.addItem("Sim�n Bolivar"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Urdaneta"); Ventana.Municipio.addItem("Zamora"); break;
	/*Monagas*/		case 16: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Acosta"); Ventana.Municipio.addItem("Aguasay"); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Cede�o"); Ventana.Municipio.addItem("Ezequiel Zamora"); Ventana.Municipio.addItem("Libertador"); Ventana.Municipio.addItem("Matur�n"); Ventana.Municipio.addItem("Piar"); Ventana.Municipio.addItem("Punceres"); Ventana.Municipio.addItem("Santa B�rbara"); Ventana.Municipio.addItem("Sotillo"); Ventana.Municipio.addItem("Uracoa"); break;
	/*NuevaEsparta*/case 17: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Antol�n del Campo"); Ventana.Municipio.addItem("Arismendi"); Ventana.Municipio.addItem("D�az"); Ventana.Municipio.addItem("Garc�a"); Ventana.Municipio.addItem("G�mez"); Ventana.Municipio.addItem("Maneiro"); Ventana.Municipio.addItem("Marcano"); Ventana.Municipio.addItem("Mari�o"); Ventana.Municipio.addItem("Pen�nsula de Macanao"); Ventana.Municipio.addItem("Tubores"); Ventana.Municipio.addItem("Villalba"); break;
	/*Portuguesa*/	case 18: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Agua Blanca"); Ventana.Municipio.addItem("Araure"); Ventana.Municipio.addItem("Esteller"); Ventana.Municipio.addItem("Guanare"); Ventana.Municipio.addItem("Guanarito"); Ventana.Municipio.addItem("Monse�or Jos� Vicenti de Unda"); Ventana.Municipio.addItem("Ospino"); Ventana.Municipio.addItem("P�ez"); Ventana.Municipio.addItem("Papel�n"); Ventana.Municipio.addItem("San Genaro de Bocono�to"); Ventana.Municipio.addItem("San Rafael de Onoto"); Ventana.Municipio.addItem("Santa Rosal�a"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Tur�n"); break;
	/*Sucre*/		case 19: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Andr�s Eloy Blanco"); Ventana.Municipio.addItem("Andr�s Mata"); Ventana.Municipio.addItem("Arismandi"); Ventana.Municipio.addItem("Ben�tez"); Ventana.Municipio.addItem("Berm�dez"); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Cajigal"); Ventana.Municipio.addItem("Cruz Salmer�n Acosta"); Ventana.Municipio.addItem("Libertador"); Ventana.Municipio.addItem("Mari�o"); Ventana.Municipio.addItem("Mej�a"); Ventana.Municipio.addItem("Montes"); Ventana.Municipio.addItem("Ribero"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Valdez"); break;
	/*Tachira*/		case 20: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Andr�s Bello"); Ventana.Municipio.addItem("Antonio R�mulo Costa"); Ventana.Municipio.addItem("Ayacucho"); Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("C�rdenas"); Ventana.Municipio.addItem("Cordoba"); Ventana.Municipio.addItem("Fernando Feo"); Ventana.Municipio.addItem("Francisco de Miranda"); Ventana.Municipio.addItem("Garcia de Hevia"); Ventana.Municipio.addItem("Gu�simos"); Ventana.Municipio.addItem("Independencia"); Ventana.Municipio.addItem("J�uregui"); Ventana.Municipio.addItem("Jos� Mar�a Vargas"); Ventana.Municipio.addItem("Jun�n"); Ventana.Municipio.addItem("San Judas Tadeo");  Ventana.Municipio.addItem("Libertad"); Ventana.Municipio.addItem("Libertador"); Ventana.Municipio.addItem("Lobatera"); Ventana.Municipio.addItem("Michelena"); Ventana.Municipio.addItem("Panamericano"); Ventana.Municipio.addItem("Pedro Mar�a Ure�a"); Ventana.Municipio.addItem("Rafael Urdaneta"); Ventana.Municipio.addItem("Samuel Dario Maldonado");  Ventana.Municipio.addItem("San Cristobal"); Ventana.Municipio.addItem("Seboruco"); Ventana.Municipio.addItem("Sim�n Rodr�guez"); Ventana.Municipio.addItem("Sucre"); Ventana.Municipio.addItem("Torbes"); Ventana.Municipio.addItem("Uribante"); break;
	/*Trujillo*/	case 21: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Andr�s Bello"); Ventana.Municipio.addItem("Bocon�");  Ventana.Municipio.addItem("Bol�var"); Ventana.Municipio.addItem("Candelaria"); Ventana.Municipio.addItem("Carache"); Ventana.Municipio.addItem("Escuque"); Ventana.Municipio.addItem("Jos� Felipe M�rquez Ca�izalez"); Ventana.Municipio.addItem("Juan Vicente Campos El�as"); Ventana.Municipio.addItem("La Ceiba"); Ventana.Municipio.addItem("Pamp�n");  Ventana.Municipio.addItem("Trujillo"); Ventana.Municipio.addItem("Andres Linares"); Ventana.Municipio.addItem("Pampanito"); break;
	/*Vargas*/		case 22: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Vargas"); break;
	/*Yaracuy*/		case 23: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Aristides Bastidas"); Ventana.Municipio.addItem("Bol�var");  Ventana.Municipio.addItem("Bruzual"); Ventana.Municipio.addItem("Cocorote"); Ventana.Municipio.addItem("Independencia"); break;
	/*Zulia*/		case 24: Ventana.Municipio.removeAllItems(); Ventana.Municipio.addItem("Almirante Padilla"); Ventana.Municipio.addItem("Baralt");  Ventana.Municipio.addItem("Cabimas"); Ventana.Municipio.addItem("Catatumbo"); Ventana.Municipio.addItem("Col�n"); Ventana.Municipio.addItem("Francisco Javier Pulgar"); Ventana.Municipio.addItem("Jes�s Enrique Losada");  Ventana.Municipio.addItem("Jes�s Mar�a Sempr�n"); break;
					default: break;
				}
			}
		}else if(Ventana.Panel4.isVisible()) {
			if(Ventana.ListaEstadoAc.getSelectedIndex() == 0)
			{
				Ventana.ListaMunicipioAc.removeAllItems(); 
				Ventana.ListaMunicipioAc.setEnabled(false);
			}
			else
			{	
				Ventana.ListaMunicipioAc.setEnabled(true);
				switch(Ventana.ListaEstadoAc.getSelectedIndex()) {
	/*D.C*/  	    case 1: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Libertador"); break;
	/*Amazonas*/	case 2: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Alto Orinoco"); Ventana.ListaMunicipioAc.addItem("Atabapo"); Ventana.ListaMunicipioAc.addItem("Altures"); Ventana.ListaMunicipioAc.addItem("Autana"); Ventana.ListaMunicipioAc.addItem("Manapiare"); Ventana.ListaMunicipioAc.addItem("Maroa"); Ventana.ListaMunicipioAc.addItem("R�o Negro"); break;
	/*Anzo�tegui*/	case 3: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Anaco"); Ventana.ListaMunicipioAc.addItem("Aragua"); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Bruzual"); Ventana.ListaMunicipioAc.addItem("Cajigal"); Ventana.ListaMunicipioAc.addItem("Carvajal"); Ventana.ListaMunicipioAc.addItem("Diego Bautista Urbaneja"); Ventana.ListaMunicipioAc.addItem("Freites"); Ventana.ListaMunicipioAc.addItem("Guanipa"); Ventana.ListaMunicipioAc.addItem("Guanta"); Ventana.ListaMunicipioAc.addItem("Independencia"); Ventana.ListaMunicipioAc.addItem("Libertad"); Ventana.ListaMunicipioAc.addItem("McGregor"); Ventana.ListaMunicipioAc.addItem("Miranda"); Ventana.ListaMunicipioAc.addItem("Monagas"); Ventana.ListaMunicipioAc.addItem("Pe�alver"); Ventana.ListaMunicipioAc.addItem("P�ritu"); Ventana.ListaMunicipioAc.addItem("San Juan de Capistrano"); Ventana.ListaMunicipioAc.addItem("Santa Ana"); Ventana.ListaMunicipioAc.addItem("Sim�n Rodriguez"); Ventana.ListaMunicipioAc.addItem("Sotillo"); break;
	/*Apure*/		case 4: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Achaguas"); Ventana.ListaMunicipioAc.addItem("Biruaca"); Ventana.ListaMunicipioAc.addItem("Mu�oz"); Ventana.ListaMunicipioAc.addItem("Pa�z"); Ventana.ListaMunicipioAc.addItem("Pedro Camejo"); Ventana.ListaMunicipioAc.addItem("R�mulo Gallegos"); Ventana.ListaMunicipioAc.addItem("San Fernando"); break;
	/*Aragua*/		case 5: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Camatagua"); Ventana.ListaMunicipioAc.addItem("Francisco Linares Alc�ntara"); Ventana.ListaMunicipioAc.addItem("Girardot"); Ventana.ListaMunicipioAc.addItem("Jos� �ngel Lamas"); Ventana.ListaMunicipioAc.addItem("Jos� F�lix Ribas"); Ventana.ListaMunicipioAc.addItem("Jos� Rafael Revenga"); Ventana.ListaMunicipioAc.addItem("Libertador"); Ventana.ListaMunicipioAc.addItem("Mario Brice�o Iragorry"); Ventana.ListaMunicipioAc.addItem("Ocumare de la Costa de Oro"); Ventana.ListaMunicipioAc.addItem("San Casimiro"); Ventana.ListaMunicipioAc.addItem("San Sebasti�n"); Ventana.ListaMunicipioAc.addItem("Santiago Mari�o"); Ventana.ListaMunicipioAc.addItem("Santos Michelena"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Tovar"); Ventana.ListaMunicipioAc.addItem("Urdaneta"); Ventana.ListaMunicipioAc.addItem("Zamora"); break;
	/*Barinas*/		case 6: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Alberto Arvelo Torrealba"); Ventana.ListaMunicipioAc.addItem("Andres Eloy Blanco"); Ventana.ListaMunicipioAc.addItem("Antonio Jos� de Sucre"); Ventana.ListaMunicipioAc.addItem("Arismendi"); Ventana.ListaMunicipioAc.addItem("Barinas"); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Cruz Paredes"); Ventana.ListaMunicipioAc.addItem("Ezequiel Zamora"); Ventana.ListaMunicipioAc.addItem("Obispos"); Ventana.ListaMunicipioAc.addItem("Pedraza"); Ventana.ListaMunicipioAc.addItem("Rojas"); Ventana.ListaMunicipioAc.addItem("Sosa"); break;
	/*Bol�var*/		case 7: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Caron�"); Ventana.ListaMunicipioAc.addItem("Cede�o"); Ventana.ListaMunicipioAc.addItem("El Callao"); Ventana.ListaMunicipioAc.addItem("Gran Sabana"); Ventana.ListaMunicipioAc.addItem("Heres"); Ventana.ListaMunicipioAc.addItem("Piar"); Ventana.ListaMunicipioAc.addItem("Ra�l Leoni"); Ventana.ListaMunicipioAc.addItem("Roscio"); Ventana.ListaMunicipioAc.addItem("Sifontes"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Padre Pedro Chen");break;
	/*Carabobo*/	case 8: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Bejuma"); Ventana.ListaMunicipioAc.addItem("Carlos Arvelo"); Ventana.ListaMunicipioAc.addItem("Diego Ibarra"); Ventana.ListaMunicipioAc.addItem("Guacara"); Ventana.ListaMunicipioAc.addItem("Juan Jos� Mora"); Ventana.ListaMunicipioAc.addItem("Libertador"); Ventana.ListaMunicipioAc.addItem("Los Guayos"); Ventana.ListaMunicipioAc.addItem("Miranda"); Ventana.ListaMunicipioAc.addItem("Montalb�n"); Ventana.ListaMunicipioAc.addItem("Naguanagua"); Ventana.ListaMunicipioAc.addItem("Puerto Cabello"); Ventana.ListaMunicipioAc.addItem("San Diego"); Ventana.ListaMunicipioAc.addItem("San Joaquin"); Ventana.ListaMunicipioAc.addItem("Valencia"); break;
	/*Cojedes*/		case 9: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Anzo�tegui"); Ventana.ListaMunicipioAc.addItem("Falc�n"); Ventana.ListaMunicipioAc.addItem("Girardot"); Ventana.ListaMunicipioAc.addItem("Lima Blanco"); break;
	/*DeltaAmacuro*/case 10: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Antonio D�az Curiapo"); Ventana.ListaMunicipioAc.addItem("Casacoima"); Ventana.ListaMunicipioAc.addItem("Perdernales"); Ventana.ListaMunicipioAc.addItem("Tucupita"); break;
	/*Falcon*/		case 11: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Acosta"); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Buchivacoa"); Ventana.ListaMunicipioAc.addItem("Cacique Manaure"); Ventana.ListaMunicipioAc.addItem("Carirubana"); Ventana.ListaMunicipioAc.addItem("Colina"); Ventana.ListaMunicipioAc.addItem("Dabajuro"); Ventana.ListaMunicipioAc.addItem("Democracia"); Ventana.ListaMunicipioAc.addItem("Falc�n"); Ventana.ListaMunicipioAc.addItem("Federaci�n"); Ventana.ListaMunicipioAc.addItem("Jacura"); Ventana.ListaMunicipioAc.addItem("Los Taques"); Ventana.ListaMunicipioAc.addItem("Mauroa"); Ventana.ListaMunicipioAc.addItem("Miranda"); Ventana.ListaMunicipioAc.addItem("Monse�or Iturriza"); Ventana.ListaMunicipioAc.addItem("Palmasola"); Ventana.ListaMunicipioAc.addItem("Petit"); Ventana.ListaMunicipioAc.addItem("P�ritu"); Ventana.ListaMunicipioAc.addItem("San Francisco"); Ventana.ListaMunicipioAc.addItem("Silva"); Ventana.ListaMunicipioAc.addItem("Toc�pero"); Ventana.ListaMunicipioAc.addItem("Uni�n"); Ventana.ListaMunicipioAc.addItem("Urumaco"); Ventana.ListaMunicipioAc.addItem("Zamora"); break;
	/*Guarico*/		case 12: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Esteros de Camaguan"); Ventana.ListaMunicipioAc.addItem("Chaguaramas"); Ventana.ListaMunicipioAc.addItem("El Socorro"); Ventana.ListaMunicipioAc.addItem("Francisco de Miranda"); Ventana.ListaMunicipioAc.addItem("Jos� F�lix Ribas"); Ventana.ListaMunicipioAc.addItem("Jos� Tadeo Monagas"); Ventana.ListaMunicipioAc.addItem("Juan Germ�n Rosci�"); Ventana.ListaMunicipioAc.addItem("Juli�n Mellado"); Ventana.ListaMunicipioAc.addItem("Las Mercedes"); Ventana.ListaMunicipioAc.addItem("Leonardo Infante"); Ventana.ListaMunicipioAc.addItem("Pedro Zaraza"); Ventana.ListaMunicipioAc.addItem("Ortiz"); Ventana.ListaMunicipioAc.addItem("San Ger�nimo del Guayabal"); Ventana.ListaMunicipioAc.addItem("San Jos� de Guaribe"); Ventana.ListaMunicipioAc.addItem("Santa Mar�a de Ipire"); break;
	/*Lara*/		case 13: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Andr�s Eloy Blanco"); Ventana.ListaMunicipioAc.addItem("Crespo"); Ventana.ListaMunicipioAc.addItem("Iribarren"); Ventana.ListaMunicipioAc.addItem("Jim�nez"); Ventana.ListaMunicipioAc.addItem("Mor�n"); Ventana.ListaMunicipioAc.addItem("Palavecino"); Ventana.ListaMunicipioAc.addItem("Sim�n Planas"); Ventana.ListaMunicipioAc.addItem("Torres"); Ventana.ListaMunicipioAc.addItem("Urdaneta"); break;
	/*Merida*/		case 14: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Alberto Adriani"); Ventana.ListaMunicipioAc.addItem("Andr�s Bello"); Ventana.ListaMunicipioAc.addItem("Antonio Pinto Salinas"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Tovar"); Ventana.ListaMunicipioAc.addItem("Tulio Febres Cordero"); Ventana.ListaMunicipioAc.addItem("Zea"); break;
	/*Miranda*/		case 15: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Acevedo"); Ventana.ListaMunicipioAc.addItem("Andr�s Bello"); Ventana.ListaMunicipioAc.addItem("Baruto"); Ventana.ListaMunicipioAc.addItem("Bri�n"); Ventana.ListaMunicipioAc.addItem("Buroz"); Ventana.ListaMunicipioAc.addItem("Carrizal"); Ventana.ListaMunicipioAc.addItem("Chacao"); Ventana.ListaMunicipioAc.addItem("Cristobal Rojas"); Ventana.ListaMunicipioAc.addItem("El Hatillo"); Ventana.ListaMunicipioAc.addItem("Gaicaipuro"); Ventana.ListaMunicipioAc.addItem("Independencia"); Ventana.ListaMunicipioAc.addItem("Lander"); Ventana.ListaMunicipioAc.addItem("Los Salias"); Ventana.ListaMunicipioAc.addItem("P�ez"); Ventana.ListaMunicipioAc.addItem("Paz Castillo");  Ventana.ListaMunicipioAc.addItem("Pedro Gual"); Ventana.ListaMunicipioAc.addItem("Plaza"); Ventana.ListaMunicipioAc.addItem("Sim�n Bolivar"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Urdaneta"); Ventana.ListaMunicipioAc.addItem("Zamora"); break;
	/*Monagas*/		case 16: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Acosta"); Ventana.ListaMunicipioAc.addItem("Aguasay"); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Cede�o"); Ventana.ListaMunicipioAc.addItem("Ezequiel Zamora"); Ventana.ListaMunicipioAc.addItem("Libertador"); Ventana.ListaMunicipioAc.addItem("Matur�n"); Ventana.ListaMunicipioAc.addItem("Piar"); Ventana.ListaMunicipioAc.addItem("Punceres"); Ventana.ListaMunicipioAc.addItem("Santa B�rbara"); Ventana.ListaMunicipioAc.addItem("Sotillo"); Ventana.ListaMunicipioAc.addItem("Uracoa"); break;
	/*NuevaEsparta*/case 17: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Antol�n del Campo"); Ventana.ListaMunicipioAc.addItem("Arismendi"); Ventana.ListaMunicipioAc.addItem("D�az"); Ventana.ListaMunicipioAc.addItem("Garc�a"); Ventana.ListaMunicipioAc.addItem("G�mez"); Ventana.ListaMunicipioAc.addItem("Maneiro"); Ventana.ListaMunicipioAc.addItem("Marcano"); Ventana.ListaMunicipioAc.addItem("Mari�o"); Ventana.ListaMunicipioAc.addItem("Pen�nsula de Macanao"); Ventana.ListaMunicipioAc.addItem("Tubores"); Ventana.ListaMunicipioAc.addItem("Villalba"); break;
	/*Portuguesa*/	case 18: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Agua Blanca"); Ventana.ListaMunicipioAc.addItem("Araure"); Ventana.ListaMunicipioAc.addItem("Esteller"); Ventana.ListaMunicipioAc.addItem("Guanare"); Ventana.ListaMunicipioAc.addItem("Guanarito"); Ventana.ListaMunicipioAc.addItem("Monse�or Jos� Vicenti de Unda"); Ventana.ListaMunicipioAc.addItem("Ospino"); Ventana.ListaMunicipioAc.addItem("P�ez"); Ventana.ListaMunicipioAc.addItem("Papel�n"); Ventana.ListaMunicipioAc.addItem("San Genaro de Bocono�to"); Ventana.ListaMunicipioAc.addItem("San Rafael de Onoto"); Ventana.ListaMunicipioAc.addItem("Santa Rosal�a"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Tur�n"); break;
	/*Sucre*/		case 19: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Andr�s Eloy Blanco"); Ventana.ListaMunicipioAc.addItem("Andr�s Mata"); Ventana.ListaMunicipioAc.addItem("Arismandi"); Ventana.ListaMunicipioAc.addItem("Ben�tez"); Ventana.ListaMunicipioAc.addItem("Berm�dez"); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Cajigal"); Ventana.ListaMunicipioAc.addItem("Cruz Salmer�n Acosta"); Ventana.ListaMunicipioAc.addItem("Libertador"); Ventana.ListaMunicipioAc.addItem("Mari�o"); Ventana.ListaMunicipioAc.addItem("Mej�a"); Ventana.ListaMunicipioAc.addItem("Montes"); Ventana.ListaMunicipioAc.addItem("Ribero"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Valdez"); break;
	/*Tachira*/		case 20: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Andr�s Bello"); Ventana.ListaMunicipioAc.addItem("Antonio R�mulo Costa"); Ventana.ListaMunicipioAc.addItem("Ayacucho"); Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("C�rdenas"); Ventana.ListaMunicipioAc.addItem("Cordoba"); Ventana.ListaMunicipioAc.addItem("Fernando Feo"); Ventana.ListaMunicipioAc.addItem("Francisco de Miranda"); Ventana.ListaMunicipioAc.addItem("Garcia de Hevia"); Ventana.ListaMunicipioAc.addItem("Gu�simos"); Ventana.ListaMunicipioAc.addItem("Independencia"); Ventana.ListaMunicipioAc.addItem("J�uregui"); Ventana.ListaMunicipioAc.addItem("Jos� Mar�a Vargas"); Ventana.ListaMunicipioAc.addItem("Jun�n"); Ventana.ListaMunicipioAc.addItem("San Judas Tadeo");  Ventana.ListaMunicipioAc.addItem("Libertad"); Ventana.ListaMunicipioAc.addItem("Libertador"); Ventana.ListaMunicipioAc.addItem("Lobatera"); Ventana.ListaMunicipioAc.addItem("Michelena"); Ventana.ListaMunicipioAc.addItem("Panamericano"); Ventana.ListaMunicipioAc.addItem("Pedro Mar�a Ure�a"); Ventana.ListaMunicipioAc.addItem("Rafael Urdaneta"); Ventana.ListaMunicipioAc.addItem("Samuel Dario Maldonado");  Ventana.ListaMunicipioAc.addItem("San Cristobal"); Ventana.ListaMunicipioAc.addItem("Seboruco"); Ventana.ListaMunicipioAc.addItem("Sim�n Rodr�guez"); Ventana.ListaMunicipioAc.addItem("Sucre"); Ventana.ListaMunicipioAc.addItem("Torbes"); Ventana.ListaMunicipioAc.addItem("Uribante"); break;
	/*Trujillo*/	case 21: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Andr�s Bello"); Ventana.ListaMunicipioAc.addItem("Bocon�");  Ventana.ListaMunicipioAc.addItem("Bol�var"); Ventana.ListaMunicipioAc.addItem("Candelaria"); Ventana.ListaMunicipioAc.addItem("Carache"); Ventana.ListaMunicipioAc.addItem("Escuque"); Ventana.ListaMunicipioAc.addItem("Jos� Felipe M�rquez Ca�izalez"); Ventana.ListaMunicipioAc.addItem("Juan Vicente Campos El�as"); Ventana.ListaMunicipioAc.addItem("La Ceiba"); Ventana.ListaMunicipioAc.addItem("Pamp�n");  Ventana.ListaMunicipioAc.addItem("Trujillo"); Ventana.ListaMunicipioAc.addItem("Andres Linares"); Ventana.ListaMunicipioAc.addItem("Pampanito"); break;
	/*Vargas*/		case 22: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Vargas"); break;
	/*Yaracuy*/		case 23: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Aristides Bastidas"); Ventana.ListaMunicipioAc.addItem("Bol�var");  Ventana.ListaMunicipioAc.addItem("Bruzual"); Ventana.ListaMunicipioAc.addItem("Cocorote"); Ventana.ListaMunicipioAc.addItem("Independencia"); break;
	/*Zulia*/		case 24: Ventana.ListaMunicipioAc.removeAllItems(); Ventana.ListaMunicipioAc.addItem("Almirante Padilla"); Ventana.ListaMunicipioAc.addItem("Baralt");  Ventana.ListaMunicipioAc.addItem("Cabimas"); Ventana.ListaMunicipioAc.addItem("Catatumbo"); Ventana.ListaMunicipioAc.addItem("Col�n"); Ventana.ListaMunicipioAc.addItem("Francisco Javier Pulgar"); Ventana.ListaMunicipioAc.addItem("Jes�s Enrique Losada");  Ventana.ListaMunicipioAc.addItem("Jes�s Mar�a Sempr�n"); break;
					default: break;
				}
			}
		}
	}

	public void EnviarModuloEstadoMunicipio() {
		
		if(Ventana.Panel2.isVisible()) {
			//Enviar el estado y municipio 
			switch(Ventana.Estado.getSelectedIndex()) {
/*D.C*/			case 1: mode.setestado(1); if(Ventana.Municipio.getSelectedIndex() == 0) {mode.setmunicipio(1);} break;
/*Amazonas*/	case 2: mode.setestado(2); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(2); break; case 1: mode.setmunicipio(3); break; case 2:mode.setmunicipio(4); break; case 3: mode.setmunicipio(5); break; case 4:mode.setmunicipio(6); break; case 5:mode.setmunicipio(7); break; case 6:mode.setmunicipio(8); break; default: break;}break;
/*Anzo�tegui*/	case 3: mode.setestado(3); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(9); break; case 1: mode.setmunicipio(10); break; case 2:mode.setmunicipio(11); break; case 3: mode.setmunicipio(12); break; case 4:mode.setmunicipio(13); break; case 5:mode.setmunicipio(14); break; case 6:mode.setmunicipio(15);  break; case 7: mode.setmunicipio(16); break; case 8: mode.setmunicipio(17); break; case 9:mode.setmunicipio(18); break; case 10: mode.setmunicipio(19); break; case 11:mode.setmunicipio(20); break; case 12:mode.setmunicipio(21); break; case 13:mode.setmunicipio(22);  break; case 14:mode.setmunicipio(23);  break; case 15: mode.setmunicipio(24); break; case 16: mode.setmunicipio(25); break; case 17:mode.setmunicipio(26); break; case 18: mode.setmunicipio(27); break; case 19:mode.setmunicipio(28); break; case 20:mode.setmunicipio(29); break; default: break;}break;
/*Apure*/		case 4: mode.setestado(4); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(30); break; case 1: mode.setmunicipio(31); break; case 2:mode.setmunicipio(32); break; case 3: mode.setmunicipio(33); break; case 4:mode.setmunicipio(34); break; case 5:mode.setmunicipio(35); break; case 6:mode.setmunicipio(36);  break; default: break;} break;
/*Aragua*/		case 5: mode.setestado(5); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(11); break; case 1: mode.setmunicipio(37); break; case 2:mode.setmunicipio(38); break; case 3: mode.setmunicipio(39); break; case 4:mode.setmunicipio(40); break; case 5:mode.setmunicipio(41); break; case 6:mode.setmunicipio(42);  break; case 7: mode.setmunicipio(1); break; case 8: mode.setmunicipio(43); break; case 9:mode.setmunicipio(44); break; case 10: mode.setmunicipio(45); break; case 11:mode.setmunicipio(46); break; case 12:mode.setmunicipio(47); break; case 13:mode.setmunicipio(48);  break; case 14:mode.setmunicipio(49);  break; case 15: mode.setmunicipio(50); break; case 16: mode.setmunicipio(51); break; case 17:mode.setmunicipio(52); break; default: break;} break;
/*Barinas*/		case 6: mode.setestado(6); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(53); break; case 1: mode.setmunicipio(54); break; case 2:mode.setmunicipio(55); break; case 3: mode.setmunicipio(56); break; case 4:mode.setmunicipio(57); break; case 5:mode.setmunicipio(11); break; case 6:mode.setmunicipio(58);  break; case 7: mode.setmunicipio(59); break; case 8: mode.setmunicipio(60); break; case 9:mode.setmunicipio(61); break; case 10: mode.setmunicipio(62); break; case 11:mode.setmunicipio(63); break; default: break;} break;
/*Bol�var*/		case 7: mode.setestado(7); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(64); break; case 1: mode.setmunicipio(65); break; case 2:mode.setmunicipio(66); break; case 3: mode.setmunicipio(67); break; case 4:mode.setmunicipio(68); break; case 5:mode.setmunicipio(69); break; case 6:mode.setmunicipio(70);  break; case 7: mode.setmunicipio(71); break; case 8: mode.setmunicipio(72); break; case 9:mode.setmunicipio(49); break; case 10: mode.setmunicipio(73); break; default: break;} break;
/*Carabobo*/	case 8: mode.setestado(8); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(74); break; case 1: mode.setmunicipio(75); break; case 2:mode.setmunicipio(76); break; case 3: mode.setmunicipio(77); break; case 4:mode.setmunicipio(78); break; case 5:mode.setmunicipio(1); break; case 6:mode.setmunicipio(79);  break; case 7: mode.setmunicipio(22); break; case 8: mode.setmunicipio(80); break; case 9:mode.setmunicipio(81); break; case 10: mode.setmunicipio(82); break; case 11:mode.setmunicipio(83); break; case 12:mode.setmunicipio(84); break; case 13:mode.setmunicipio(85);  break; default: break;} break;
/*Cojedes*/		case 9: mode.setestado(9); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(86); break; case 1: mode.setmunicipio(87); break; case 2:mode.setmunicipio(39); break; case 3: mode.setmunicipio(88); break; case 4:mode.setmunicipio(89); break; case 5:mode.setmunicipio(90); break; case 6:mode.setmunicipio(35);  break; default: break;} break;
/*DeltaAmacuro*/case 10: mode.setestado(10); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(91); break; case 1: mode.setmunicipio(92); break; case 2:mode.setmunicipio(93); break; case 3: mode.setmunicipio(94); break; default: break;} break;
/*Falcon*/		case 11: mode.setestado(11); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(95); break; case 1: mode.setmunicipio(11); break; case 2:mode.setmunicipio(96); break; case 3: mode.setmunicipio(97); break; case 4:mode.setmunicipio(98); break; case 5:mode.setmunicipio(99); break; case 6:mode.setmunicipio(100);  break; case 7: mode.setmunicipio(101); break; case 8: mode.setmunicipio(87); break; case 9: mode.setmunicipio(102); break; case 10:mode.setmunicipio(103); break; case 11: mode.setmunicipio(104); break; case 12:mode.setmunicipio(105); break; case 13:mode.setmunicipio(22); break; case 14:mode.setmunicipio(106);  break; case 15:mode.setmunicipio(107);  break; case 16: mode.setmunicipio(108); break; case 17: mode.setmunicipio(25); break; case 18:mode.setmunicipio(109); break; case 19: mode.setmunicipio(110); break; case 20:mode.setmunicipio(49); break; case 21:mode.setmunicipio(111); break; case 22:mode.setmunicipio(112); break; case 23: mode.setmunicipio(113); break; case 24:mode.setmunicipio(52); break; default: break;}break;
/*Guarico*/		case 12: mode.setestado(12); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(114); break; case 1: mode.setmunicipio(115); break; case 2:mode.setmunicipio(116); break; case 3: mode.setmunicipio(117); break; case 4: mode.setmunicipio(41); case 5:mode.setmunicipio(118); break; case 6:mode.setmunicipio(119); break; case 7:mode.setmunicipio(120);  break; case 8: mode.setmunicipio(121); break; case 9: mode.setmunicipio(122); break; case 10: mode.setmunicipio(123); break; case 11:mode.setmunicipio(124); break; case 12: mode.setmunicipio(125); break; case 13:mode.setmunicipio(126); break; case 14: mode.setmunicipio(127); break; default: break;} break;
/*Lara*/		case 13: mode.setestado(13); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(54); break; case 1: mode.setmunicipio(128); break; case 2:mode.setmunicipio(129); break; case 3: mode.setmunicipio(130); break; case 4: mode.setmunicipio(131); case 5:mode.setmunicipio(132); break; case 6:mode.setmunicipio(133); break; case 7:mode.setmunicipio(134);  break; case 8: mode.setmunicipio(51); break;} break;
/*Merida*/		case 14: mode.setestado(14); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(135); break; case 1: mode.setmunicipio(136); break; case 2:mode.setmunicipio(137); break; case 3: mode.setmunicipio(49); break; case 4: mode.setmunicipio(50); case 5:mode.setmunicipio(138); break; case 6:mode.setmunicipio(139); break;} break;
/*Miranda*/		case 15: mode.setestado(15); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(140); break; case 1: mode.setmunicipio(136); break; case 2:mode.setmunicipio(141); break; case 3: mode.setmunicipio(142); break; case 4:mode.setmunicipio(143); break; case 5:mode.setmunicipio(144); break; case 6:mode.setmunicipio(145);  break; case 7: mode.setmunicipio(146); break; case 8: mode.setmunicipio(147); break; case 9:mode.setmunicipio(148); break; case 10: mode.setmunicipio(19); break; case 11:mode.setmunicipio(149); break; case 12:mode.setmunicipio(150); break; case 13:mode.setmunicipio(33);  break; case 14:mode.setmunicipio(151);  break; case 15: mode.setmunicipio(152); break; case 16: mode.setmunicipio(153); break; case 17:mode.setmunicipio(154); break; case 18: mode.setmunicipio(49); break; case 19:mode.setmunicipio(51); break; case 20:mode.setmunicipio(52); break; default: break;}break;
/*Monagas*/		case 16: mode.setestado(16); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(95); break; case 1: mode.setmunicipio(155); break; case 2:mode.setmunicipio(11); break; case 3: mode.setmunicipio(65); break; case 4:mode.setmunicipio(59); break; case 5:mode.setmunicipio(1); break; case 6:mode.setmunicipio(156);  break; case 7: mode.setmunicipio(69); break; case 8: mode.setmunicipio(157); break; case 9:mode.setmunicipio(158); break; case 10: mode.setmunicipio(159); break; case 11:mode.setmunicipio(160); break;} break;
/*NuevaEsparta*/case 17: mode.setestado(17); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(161); break; case 1: mode.setmunicipio(56); break; case 2:mode.setmunicipio(162); break; case 3: mode.setmunicipio(163); break; case 4:mode.setmunicipio(164); break; case 5:mode.setmunicipio(165); break; case 6:mode.setmunicipio(166);  break; case 7: mode.setmunicipio(167); break; case 8: mode.setmunicipio(168); break; case 9:mode.setmunicipio(169); break; case 10: mode.setmunicipio(170); break;} break;
/*Portuguesa*/	case 18: mode.setestado(18); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(171); break; case 1: mode.setmunicipio(172); break; case 2:mode.setmunicipio(173); break; case 3: mode.setmunicipio(174); break; case 4:mode.setmunicipio(175); break; case 5:mode.setmunicipio(176); break; case 6:mode.setmunicipio(177);  break; case 7: mode.setmunicipio(33); break; case 8: mode.setmunicipio(178); break; case 9:mode.setmunicipio(179); break; case 10: mode.setmunicipio(180); break; case 11:mode.setmunicipio(181); break; case 12:mode.setmunicipio(49); break; case 13:mode.setmunicipio(182);  break;}break;
/*Sucre*/		case 19: mode.setestado(19); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(54); break; case 1: mode.setmunicipio(183); break; case 2:mode.setmunicipio(56); break; case 3: mode.setmunicipio(184); break; case 4:mode.setmunicipio(185); break; case 5:mode.setmunicipio(11); break; case 6:mode.setmunicipio(186);  break; case 7: mode.setmunicipio(187); break; case 8: mode.setmunicipio(1); break; case 9:mode.setmunicipio(167); break; case 10: mode.setmunicipio(188); break; case 11:mode.setmunicipio(189); break; case 12:mode.setmunicipio(190); break; case 13:mode.setmunicipio(49);  break; case 14:mode.setmunicipio(191);  break;} break;
/*Tachira*/		case 20: mode.setestado(20); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(136); break; case 1: mode.setmunicipio(192); break; case 2:mode.setmunicipio(193); break; case 3: mode.setmunicipio(11); break; case 4:mode.setmunicipio(194); break; case 5:mode.setmunicipio(195); break; case 6:mode.setmunicipio(196);  break; case 7: mode.setmunicipio(117); break; case 8: mode.setmunicipio(197); break; case 9: mode.setmunicipio(198); break; case 10:mode.setmunicipio(19); break; case 11: mode.setmunicipio(199); break; case 12:mode.setmunicipio(200); break; case 13:mode.setmunicipio(201); break; case 14:mode.setmunicipio(202);  break; case 15:mode.setmunicipio(203);  break; case 16: mode.setmunicipio(1); break; case 17: mode.setmunicipio(204); break; case 18:mode.setmunicipio(205); break; case 19: mode.setmunicipio(206); break; case 20:mode.setmunicipio(207); break; case 21:mode.setmunicipio(208); break; case 22:mode.setmunicipio(209); break; case 23: mode.setmunicipio(210); break; case 24:mode.setmunicipio(211); break; case 25: mode.setmunicipio(212); break; case 26:mode.setmunicipio(49); break; case 27: mode.setmunicipio(213); break; case 28:mode.setmunicipio(214); break; default: break;}break;
/*Trujillo*/	case 21: mode.setestado(21); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(136); break; case 1: mode.setmunicipio(215); break; case 2:mode.setmunicipio(11); break; case 3: mode.setmunicipio(216); break; case 4:mode.setmunicipio(217); break; case 5:mode.setmunicipio(218); break; case 6:mode.setmunicipio(219);  break; case 7: mode.setmunicipio(220); break; case 8: mode.setmunicipio(221); break; case 9: mode.setmunicipio(22); break; case 10:mode.setmunicipio(222); break; case 11: mode.setmunicipio(223); break; case 12:mode.setmunicipio(224); break; case 13:mode.setmunicipio(225); break;} break;
/*Vargas*/		case 22: mode.setestado(22); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(226); break;}break;
/*Yaracuy*/		case 23: mode.setestado(23); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(227); break; case 1: mode.setmunicipio(11); break; case 2:mode.setmunicipio(228); break; case 3: mode.setmunicipio(229); break; case 4:mode.setmunicipio(19);}break;
/*Zulia*/		case 24: mode.setestado(24); switch(Ventana.Municipio.getSelectedIndex()) { case 0: mode.setmunicipio(230); break; case 1: mode.setmunicipio(231); break; case 2:mode.setmunicipio(232); break; case 3: mode.setmunicipio(233); break; case 4:mode.setmunicipio(234); break; case 5:mode.setmunicipio(235); break; case 6:mode.setmunicipio(236);  break; case 7: mode.setmunicipio(237); break;}break;
				default: break;
			}	
		} else if (Ventana.Panel4.isVisible()) {
			//Enviar el estado y municipio 
			switch(Ventana.ListaEstadoAc.getSelectedIndex()) {
/*D.C*/			case 1: mode.setestado(1); if(Ventana.ListaMunicipioAc.getSelectedIndex() == 0) {mode.setmunicipio(1);} break;
/*Amazonas*/	case 2: mode.setestado(2); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(2); break; case 1: mode.setmunicipio(3); break; case 2:mode.setmunicipio(4); break; case 3: mode.setmunicipio(5); break; case 4:mode.setmunicipio(6); break; case 5:mode.setmunicipio(7); break; case 6:mode.setmunicipio(8); break; default: break;}break;
/*Anzo�tegui*/	case 3: mode.setestado(3); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(9); break; case 1: mode.setmunicipio(10); break; case 2:mode.setmunicipio(11); break; case 3: mode.setmunicipio(12); break; case 4:mode.setmunicipio(13); break; case 5:mode.setmunicipio(14); break; case 6:mode.setmunicipio(15);  break; case 7: mode.setmunicipio(16); break; case 8: mode.setmunicipio(17); break; case 9:mode.setmunicipio(18); break; case 10: mode.setmunicipio(19); break; case 11:mode.setmunicipio(20); break; case 12:mode.setmunicipio(21); break; case 13:mode.setmunicipio(22);  break; case 14:mode.setmunicipio(23);  break; case 15: mode.setmunicipio(24); break; case 16: mode.setmunicipio(25); break; case 17:mode.setmunicipio(26); break; case 18: mode.setmunicipio(27); break; case 19:mode.setmunicipio(28); break; case 20:mode.setmunicipio(29); break; default: break;}break;
/*Apure*/		case 4: mode.setestado(4); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(30); break; case 1: mode.setmunicipio(31); break; case 2:mode.setmunicipio(32); break; case 3: mode.setmunicipio(33); break; case 4:mode.setmunicipio(34); break; case 5:mode.setmunicipio(35); break; case 6:mode.setmunicipio(36);  break; default: break;} break;
/*Aragua*/		case 5: mode.setestado(5); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(11); break; case 1: mode.setmunicipio(37); break; case 2:mode.setmunicipio(38); break; case 3: mode.setmunicipio(39); break; case 4:mode.setmunicipio(40); break; case 5:mode.setmunicipio(41); break; case 6:mode.setmunicipio(42);  break; case 7: mode.setmunicipio(1); break; case 8: mode.setmunicipio(43); break; case 9:mode.setmunicipio(44); break; case 10: mode.setmunicipio(45); break; case 11:mode.setmunicipio(46); break; case 12:mode.setmunicipio(47); break; case 13:mode.setmunicipio(48);  break; case 14:mode.setmunicipio(49);  break; case 15: mode.setmunicipio(50); break; case 16: mode.setmunicipio(51); break; case 17:mode.setmunicipio(52); break; default: break;} break;
/*Barinas*/		case 6: mode.setestado(6); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(53); break; case 1: mode.setmunicipio(54); break; case 2:mode.setmunicipio(55); break; case 3: mode.setmunicipio(56); break; case 4:mode.setmunicipio(57); break; case 5:mode.setmunicipio(11); break; case 6:mode.setmunicipio(58);  break; case 7: mode.setmunicipio(59); break; case 8: mode.setmunicipio(60); break; case 9:mode.setmunicipio(61); break; case 10: mode.setmunicipio(62); break; case 11:mode.setmunicipio(63); break; default: break;} break;
/*Bol�var*/		case 7: mode.setestado(7); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(64); break; case 1: mode.setmunicipio(65); break; case 2:mode.setmunicipio(66); break; case 3: mode.setmunicipio(67); break; case 4:mode.setmunicipio(68); break; case 5:mode.setmunicipio(69); break; case 6:mode.setmunicipio(70);  break; case 7: mode.setmunicipio(71); break; case 8: mode.setmunicipio(72); break; case 9:mode.setmunicipio(49); break; case 10: mode.setmunicipio(73); break; default: break;} break;
/*Carabobo*/	case 8: mode.setestado(8); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(74); break; case 1: mode.setmunicipio(75); break; case 2:mode.setmunicipio(76); break; case 3: mode.setmunicipio(77); break; case 4:mode.setmunicipio(78); break; case 5:mode.setmunicipio(1); break; case 6:mode.setmunicipio(79);  break; case 7: mode.setmunicipio(22); break; case 8: mode.setmunicipio(80); break; case 9:mode.setmunicipio(81); break; case 10: mode.setmunicipio(82); break; case 11:mode.setmunicipio(83); break; case 12:mode.setmunicipio(84); break; case 13:mode.setmunicipio(85);  break; default: break;} break;
/*Cojedes*/		case 9: mode.setestado(9); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(86); break; case 1: mode.setmunicipio(87); break; case 2:mode.setmunicipio(39); break; case 3: mode.setmunicipio(88); break; case 4:mode.setmunicipio(89); break; case 5:mode.setmunicipio(90); break; case 6:mode.setmunicipio(35);  break; default: break;} break;
/*DeltaAmacuro*/case 10: mode.setestado(10); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(91); break; case 1: mode.setmunicipio(92); break; case 2:mode.setmunicipio(93); break; case 3: mode.setmunicipio(94); break; default: break;} break;
/*Falcon*/		case 11: mode.setestado(11); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(95); break; case 1: mode.setmunicipio(11); break; case 2:mode.setmunicipio(96); break; case 3: mode.setmunicipio(97); break; case 4:mode.setmunicipio(98); break; case 5:mode.setmunicipio(99); break; case 6:mode.setmunicipio(100);  break; case 7: mode.setmunicipio(101); break; case 8: mode.setmunicipio(87); break; case 9: mode.setmunicipio(102); break; case 10:mode.setmunicipio(103); break; case 11: mode.setmunicipio(104); break; case 12:mode.setmunicipio(105); break; case 13:mode.setmunicipio(22); break; case 14:mode.setmunicipio(106);  break; case 15:mode.setmunicipio(107);  break; case 16: mode.setmunicipio(108); break; case 17: mode.setmunicipio(25); break; case 18:mode.setmunicipio(109); break; case 19: mode.setmunicipio(110); break; case 20:mode.setmunicipio(49); break; case 21:mode.setmunicipio(111); break; case 22:mode.setmunicipio(112); break; case 23: mode.setmunicipio(113); break; case 24:mode.setmunicipio(52); break; default: break;}break;
/*Guarico*/		case 12: mode.setestado(12); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(114); break; case 1: mode.setmunicipio(115); break; case 2:mode.setmunicipio(116); break; case 3: mode.setmunicipio(117); break; case 4: mode.setmunicipio(41); case 5:mode.setmunicipio(118); break; case 6:mode.setmunicipio(119); break; case 7:mode.setmunicipio(120);  break; case 8: mode.setmunicipio(121); break; case 9: mode.setmunicipio(122); break; case 10: mode.setmunicipio(123); break; case 11:mode.setmunicipio(124); break; case 12: mode.setmunicipio(125); break; case 13:mode.setmunicipio(126); break; case 14: mode.setmunicipio(127); break; default: break;} break;
/*Lara*/		case 13: mode.setestado(13); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(54); break; case 1: mode.setmunicipio(128); break; case 2:mode.setmunicipio(129); break; case 3: mode.setmunicipio(130); break; case 4: mode.setmunicipio(131); case 5:mode.setmunicipio(132); break; case 6:mode.setmunicipio(133); break; case 7:mode.setmunicipio(134);  break; case 8: mode.setmunicipio(51); break;} break;
/*Merida*/		case 14: mode.setestado(14); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(135); break; case 1: mode.setmunicipio(136); break; case 2:mode.setmunicipio(137); break; case 3: mode.setmunicipio(49); break; case 4: mode.setmunicipio(50); case 5:mode.setmunicipio(138); break; case 6:mode.setmunicipio(139); break;} break;
/*Miranda*/		case 15: mode.setestado(15); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(140); break; case 1: mode.setmunicipio(136); break; case 2:mode.setmunicipio(141); break; case 3: mode.setmunicipio(142); break; case 4:mode.setmunicipio(143); break; case 5:mode.setmunicipio(144); break; case 6:mode.setmunicipio(145);  break; case 7: mode.setmunicipio(146); break; case 8: mode.setmunicipio(147); break; case 9:mode.setmunicipio(148); break; case 10: mode.setmunicipio(19); break; case 11:mode.setmunicipio(149); break; case 12:mode.setmunicipio(150); break; case 13:mode.setmunicipio(33);  break; case 14:mode.setmunicipio(151);  break; case 15: mode.setmunicipio(152); break; case 16: mode.setmunicipio(153); break; case 17:mode.setmunicipio(154); break; case 18: mode.setmunicipio(49); break; case 19:mode.setmunicipio(51); break; case 20:mode.setmunicipio(52); break; default: break;}break;
/*Monagas*/		case 16: mode.setestado(16); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(95); break; case 1: mode.setmunicipio(155); break; case 2:mode.setmunicipio(11); break; case 3: mode.setmunicipio(65); break; case 4:mode.setmunicipio(59); break; case 5:mode.setmunicipio(1); break; case 6:mode.setmunicipio(156);  break; case 7: mode.setmunicipio(69); break; case 8: mode.setmunicipio(157); break; case 9:mode.setmunicipio(158); break; case 10: mode.setmunicipio(159); break; case 11:mode.setmunicipio(160); break;} break;
/*NuevaEsparta*/case 17: mode.setestado(17); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(161); break; case 1: mode.setmunicipio(56); break; case 2:mode.setmunicipio(162); break; case 3: mode.setmunicipio(163); break; case 4:mode.setmunicipio(164); break; case 5:mode.setmunicipio(165); break; case 6:mode.setmunicipio(166);  break; case 7: mode.setmunicipio(167); break; case 8: mode.setmunicipio(168); break; case 9:mode.setmunicipio(169); break; case 10: mode.setmunicipio(170); break;} break;
/*Portuguesa*/	case 18: mode.setestado(18); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(171); break; case 1: mode.setmunicipio(172); break; case 2:mode.setmunicipio(173); break; case 3: mode.setmunicipio(174); break; case 4:mode.setmunicipio(175); break; case 5:mode.setmunicipio(176); break; case 6:mode.setmunicipio(177);  break; case 7: mode.setmunicipio(33); break; case 8: mode.setmunicipio(178); break; case 9:mode.setmunicipio(179); break; case 10: mode.setmunicipio(180); break; case 11:mode.setmunicipio(181); break; case 12:mode.setmunicipio(49); break; case 13:mode.setmunicipio(182);  break;}break;
/*Sucre*/		case 19: mode.setestado(19); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(54); break; case 1: mode.setmunicipio(183); break; case 2:mode.setmunicipio(56); break; case 3: mode.setmunicipio(184); break; case 4:mode.setmunicipio(185); break; case 5:mode.setmunicipio(11); break; case 6:mode.setmunicipio(186);  break; case 7: mode.setmunicipio(187); break; case 8: mode.setmunicipio(1); break; case 9:mode.setmunicipio(167); break; case 10: mode.setmunicipio(188); break; case 11:mode.setmunicipio(189); break; case 12:mode.setmunicipio(190); break; case 13:mode.setmunicipio(49);  break; case 14:mode.setmunicipio(191);  break;} break;
/*Tachira*/		case 20: mode.setestado(20); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(136); break; case 1: mode.setmunicipio(192); break; case 2:mode.setmunicipio(193); break; case 3: mode.setmunicipio(11); break; case 4:mode.setmunicipio(194); break; case 5:mode.setmunicipio(195); break; case 6:mode.setmunicipio(196);  break; case 7: mode.setmunicipio(117); break; case 8: mode.setmunicipio(197); break; case 9: mode.setmunicipio(198); break; case 10:mode.setmunicipio(19); break; case 11: mode.setmunicipio(199); break; case 12:mode.setmunicipio(200); break; case 13:mode.setmunicipio(201); break; case 14:mode.setmunicipio(202);  break; case 15:mode.setmunicipio(203);  break; case 16: mode.setmunicipio(1); break; case 17: mode.setmunicipio(204); break; case 18:mode.setmunicipio(205); break; case 19: mode.setmunicipio(206); break; case 20:mode.setmunicipio(207); break; case 21:mode.setmunicipio(208); break; case 22:mode.setmunicipio(209); break; case 23: mode.setmunicipio(210); break; case 24:mode.setmunicipio(211); break; case 25: mode.setmunicipio(212); break; case 26:mode.setmunicipio(49); break; case 27: mode.setmunicipio(213); break; case 28:mode.setmunicipio(214); break; default: break;}break;
/*Trujillo*/	case 21: mode.setestado(21); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(136); break; case 1: mode.setmunicipio(215); break; case 2:mode.setmunicipio(11); break; case 3: mode.setmunicipio(216); break; case 4:mode.setmunicipio(217); break; case 5:mode.setmunicipio(218); break; case 6:mode.setmunicipio(219);  break; case 7: mode.setmunicipio(220); break; case 8: mode.setmunicipio(221); break; case 9: mode.setmunicipio(22); break; case 10:mode.setmunicipio(222); break; case 11: mode.setmunicipio(223); break; case 12:mode.setmunicipio(224); break; case 13:mode.setmunicipio(225); break;} break;
/*Vargas*/		case 22: mode.setestado(22); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(226); break;}break;
/*Yaracuy*/		case 23: mode.setestado(23); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(227); break; case 1: mode.setmunicipio(11); break; case 2:mode.setmunicipio(228); break; case 3: mode.setmunicipio(229); break; case 4:mode.setmunicipio(19);}break;
/*Zulia*/		case 24: mode.setestado(24); switch(Ventana.ListaMunicipioAc.getSelectedIndex()) { case 0: mode.setmunicipio(230); break; case 1: mode.setmunicipio(231); break; case 2:mode.setmunicipio(232); break; case 3: mode.setmunicipio(233); break; case 4:mode.setmunicipio(234); break; case 5:mode.setmunicipio(235); break; case 6:mode.setmunicipio(236);  break; case 7: mode.setmunicipio(237); break;}break;
				default: break;
			}	
		}
	}
}
