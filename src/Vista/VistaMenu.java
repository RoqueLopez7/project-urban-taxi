package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controlador.ControladorPrincipal;

public class VistaMenu extends JPanel{

	private JLabel Bienvenido, Subtitulo, Urban, Taxi;
	public JButton Ingresar,Buscar,Actualizar, Eliminar, Salir;
	
	public VistaMenu() {
		
		setLayout(null);
		IniciarComponentes();
		Etiquetas();
		Botones();
	}
	
	public void IniciarComponentes(){

		//Vista de la ventana
		setBounds(0,0,1080,720);
		setBackground(new Color(87,35,100));
	}

	public void Etiquetas() {
		
		//Bienvenida
		Bienvenido = new JLabel("Bienvenido al Gestor de Datos");
		Bienvenido.setBounds(200,10,700,100);
		Bienvenido.setFont(new Font("Arial", 0, 50));
		Bienvenido.setForeground(new Color(250,250,250));
		add(Bienvenido);
		
		//Subtitulo
		Subtitulo = new JLabel("Menu");
		Subtitulo.setBounds(222,135,500,50);
		Subtitulo.setFont(new Font("Arial", 0, 30));
		Subtitulo.setForeground(new Color(230,230,230));
		add(Subtitulo);
		
		//Urban
		Urban = new JLabel("Urban");
		Urban.setFont(new Font("Harlow Solid Italic", 1, 130));
		Urban.setBounds(460, 215, 500,200);
		Urban.setForeground(new Color(255,255,0));
		add(Urban);
		
		//Taxi
		Taxi = new JLabel("Taxi");
		Taxi.setFont(new Font("Harlow Solid Italic", 1, 130));
		Taxi.setBounds(650, 325, 500,200);
		Taxi.setForeground(new Color(255,255,0));
		add(Taxi);
	}

	public void Botones() {
		
		//Ingresar Conductor
		Ingresar = new JButton("Ingresar un Conductor");
		Ingresar.setBounds(150,200, 220,70);
		Ingresar.setFont(new Font("Arial", 1, 16));
		add(Ingresar);
		
		//Buscar Conductor
		Buscar = new JButton("Buscar un Conductor");
		Buscar.setBounds(150,300,220,70);
		Buscar.setFont(new Font("Arial", 1, 16));
		add(Buscar);
		
		//Actualizar Conductor
		Actualizar = new JButton("Actualizar un Conductor");
		Actualizar.setBounds(150,400,220,70);
		Actualizar.setFont(new Font("Arial", 1, 16));
		add(Actualizar);
		
		//Eliminar Conductor
		Eliminar = new JButton("Eliminar un Conductor");
		Eliminar.setBounds(150,500,220,70);
		Eliminar.setFont(new Font("Arial", 1, 16));
		add(Eliminar);
		
		//Salir
		Salir = new JButton("Salir");
		Salir.setBounds(150,600,220,70);
		Salir.setFont(new Font("Arial", 1, 16));
		add(Salir);		
		
		//Conectar con el controlador
		/*ControladorPrincipal controlprincipal = new ControladorPrincipal(this);
		
		//A�adiendo sus oyentes
		Ingresar.addActionListener(controlprincipal);
		Buscar.addActionListener(controlprincipal);
		Actualizar.addActionListener(controlprincipal);
		Eliminar.addActionListener(controlprincipal);
		Salir.addActionListener(controlprincipal);*/
	}

}
