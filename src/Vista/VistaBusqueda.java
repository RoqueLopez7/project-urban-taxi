package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;

public class VistaBusqueda extends JPanel {

	//Componentes
	JLabel Busqueda, IngreseCI;
	JTextField InputBus;
	JButton Buscar, Regresar;
	JTextArea Area;
	
	VistaIngresar Vingresar;
	
	public VistaBusqueda() {
		
		setLayout(null);
		setBounds(0,0,1080,720);
		setBackground(new Color(87,35,100));
		EtiquetasBusqueda();
		InputBusqueda();
		BotonesBusqueda();
		TablaBusqueda();
	}
	
	public void EtiquetasBusqueda() {
		
		//Busqueda de un conductor
		Busqueda = new JLabel("Busqueda de un conductor");
		Busqueda.setBounds(275,5,800,50);
		Busqueda.setFont(new Font("Harlow Solid Italic", 1, 40));
		Busqueda.setForeground(new Color(250,250,0));
		add(Busqueda);
		
		//Ingrese la C.I del conductor
		IngreseCI = new JLabel("Ingrese la C.I del conductor");
		IngreseCI.setBounds(100,150,400,30);
		IngreseCI.setFont(new Font("Arial", 0, 20));
		IngreseCI.setForeground(new Color(100,230,100));
		add(IngreseCI);
	}
	
	public void InputBusqueda() {
		
		InputBus = new JTextField();
		InputBus.setBounds(100,190,245,30);
		add(InputBus);
	}
	
	public void BotonesBusqueda() {
		//Buscar
		Buscar = new JButton("Buscar");
		Buscar.setBounds(360, 180, 150, 50);
		Buscar.setFont(new Font("Arial", 1,20));
		add(Buscar);
		
		//Regresar
		Regresar = new JButton("Regresar");
		Regresar.setBounds(5,5,90,40);
		Regresar.setFont(new Font("Arial", 1, 12));
		add(Regresar);
		
		//Accion para los botones
		//Buscar.addActionListener(controladorbusqueda);
		//Regresar.addActionListener(controladorbusqueda);
		
	}
	
	public void TablaBusqueda() {
		
		Area = new JTextArea();
		Area.setBounds(150,300,800,340);
		Area.setFont(new Font("Calibri", 1, 17));
		Area.setText("Nombre y Apellidos: Roque Lopez.\n"+"Cedula: 28012038.\n"+"Direccion: San Juan de los Morros.\n"+
		"Telefono: 041203411367.\n"+"Correo: roquel371gmail.com\n"+"Estado: Guarico.\n"+"Municipio: Juan German Roscio.\n"+
		"Placa del vehiculo: DBS ACDC.\n"+"Modelo del vehiculo: Ford.\n"+"Marca del vehiculo: Fiesta.");
		add(Area);
	}
}
