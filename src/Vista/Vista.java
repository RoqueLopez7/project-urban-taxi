package Vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyListener;

import javax.swing.*;

import Controlador.ControladorPrincipal;

public class Vista extends JFrame {
	
	/**
	 * No s� para que es esto pero hace que se le quite la advertencia a la clase
	 * asi que para mi est� bien, y no le hace da�o al programa xD
	 */
	private static final long serialVersionUID = 1L;

	//Paneles
	public JPanel Panel1,Panel2, Panel3, Panel4, Panel5;
	
	//Componentes del menu
	private JLabel Bienvenido, Subtitulo, Urban, Taxi, Otro;
	public JButton Ingresar, Buscar, Actualizar, Eliminar, Salir, Reporte;
	
	//Componentes del ingresar
	public JLabel Ingrese, DatosConductor, DatosVehiculo, Nombre, CI, dir, telef, correo, estado,municipio, placa, modelo, marca;
	public JTextField NombreC, Cedula,Direccion,Tlf,CorreoE,PlacaVe,ModeloVe,MarcaVe;
	public JButton Enviar, Regresar;
	public JComboBox Estado, Municipio;
		
	//opciones de la lista de estado
		public String [] estados = {"","Distrito Capital", "Amazonas", "Anzo�tegui","Apure","Aragua","Barinas","Bol�var","Carabobo","Cojedes","Delta Amacuro","Falc�n","Gu�rico","Lara","Merida","Miranda","Monagas","Nueva Esparta", "Portuguesa", "Sucre","Tachira","Trujillo","Vargas","Yaracuy","Zulia"};

	//Componentes del busqueda
	public JLabel Busqueda, IngreseCI;
	public JTextField InputBus;
	public JTextArea Area;
	public JButton BuscarConductor,ConsultaGeneral, Regresar2;

	//Componentes del Actualizar
	public JLabel Actualizacion, IngreseCI2, DatosConductorAc, DatosVehiculoAc, NombreAc, CIAc, dirAc, telefAc, correoAc, estadoAc,municipioAc, placaAc, modeloAc, marcaAc;
	public JTextField InputAct, NombreCAc, CedulaAc,DireccionAc,TlfAc,CorreoEAc,EstadoAc,MunicipioAc,PlacaVeAc,ModeloVeAc,MarcaVeAc;
	public JButton BuscarAct, Regresar3, ActualizarAc;
	public JTextArea AreaAct;
	public JComboBox ListaEstadoAc, ListaMunicipioAc;
	
	//Componentes del Borrar
	public JLabel BorrarCo, IngreseCI3, DeseaBor;
	public JTextField InputBor;
	public JButton BuscarBor, Regresar4, Afirmar, Negar;
	public JTextArea AreaBor;
	
	//Conectar con el controlador
	ControladorPrincipal controlprincipal;
	
	public Vista() {

		//Vista de la ventana
		
		setLayout(null);
		this.setVisible(true);
		setBounds(0,0,1080,720);
		setBackground(new Color(87,35,100));
		this.setTitle("Gestor de Datos - Urban Taxi");
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		//Llamada del metodo
		
		//Asignando el controlador para los oyentes de accion
		controlprincipal = new ControladorPrincipal(this);
		
		//Menu
		IniciarComponentes();
		Etiquetas();
		Botones();
		
		//Ingresar
		EtiquetasIngresar();
		InputsIngresar();
		BotonesIngresar();
		
		//Busqueda
		EtiquetasBusqueda();
		InputBusqueda();
		BotonesBusqueda();
		TablaBusqueda();

		//Actualizar
		EtiquetasActualizar();
		InputActualizar();
		BotonesActualizar();
		
		//Borrar
		EtiquetasBorrar();
		InputBorrar();
		BotonesBorrar();
		
		//Terminar proceso
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	
	//------->Inicio de los paneles<-------
	
	public void IniciarComponentes() {
		
		//Icono para el programa
		setIconImage(new ImageIcon(getClass().getResource("../img/Taxi.png")).getImage()); //Estableciendo un icono a la ventana
		
		//Panel 1 Vista del Menu
		Panel1 = new JPanel();
		Panel1.setBounds(0,0,1080,720);
		Panel1.setBackground(new Color(87,35,100));
		Panel1.setLayout(null);
		Panel1.setVisible(true);
		add(Panel1);
		
		//Panel 2 - Vista del ingresar
		Panel2 = new JPanel();
		Panel2.setBounds(0,0,1080,720);
		Panel2.setBackground(new Color(87,35,100));
		Panel2.setLayout(null);
		Panel2.setVisible(false);
		add(Panel2);
		
		//Panel 3 - Vista de la busqueda
		Panel3 = new JPanel();
		Panel3.setBounds(0,0,1080,720);
		Panel3.setBackground(new Color(87,35,100));
		Panel3.setLayout(null);
		Panel3.setVisible(false);
		add(Panel3);
		
		//Panel 4 - Vista del Actualizar
		Panel4 = new JPanel();
		Panel4.setBounds(0,0,1080,720);
		Panel4.setBackground(new Color(87,35,100));
		Panel4.setLayout(null);
		Panel4.setVisible(false);
		add(Panel4);
		
		//Panel 5 - Vista del Borrar
		Panel5= new JPanel();
		Panel5.setBounds(0,0,1080,720);
		Panel5.setBackground(new Color(87,35,100));
		Panel5.setLayout(null);
		Panel5.setVisible(false);
		add(Panel5);
	}

	//------->Vista del menu<-------
	
	public void Etiquetas() {
		
		//Bienvenida
		Bienvenido = new JLabel("Bienvenido al Gestor de Datos");
		Bienvenido.setBounds(200,10,700,100);
		Bienvenido.setFont(new Font("Arial", 0, 50));
		Bienvenido.setForeground(new Color(250,250,250));
		Panel1.add(Bienvenido);
		
		//Menu
		Subtitulo = new JLabel("Menu");
		Subtitulo.setBounds(190,105,500,50);
		Subtitulo.setFont(new Font("Arial", 0, 30));
		Subtitulo.setForeground(new Color(230,230,230));
		Panel1.add(Subtitulo);
		
		//Urban
		Urban = new JLabel("Urban");
		Urban.setFont(new Font("Harlow Solid Italic", 1, 130));
		Urban.setBounds(460, 215, 500,200);
		Urban.setForeground(new Color(255,255,0));
		Panel1.add(Urban);
		
		//Taxi
		Taxi = new JLabel("Taxi");
		Taxi.setFont(new Font("Harlow Solid Italic", 1, 130));
		Taxi.setBounds(650, 325, 500,200);
		Taxi.setForeground(new Color(255,255,0));
		Panel1.add(Taxi);
		
		//Otro
		Otro = new JLabel("Otro");
		Otro.setBounds(850,105,500,50);
		Otro.setFont(new Font("Arial", 0, 25));
		Otro.setForeground(new Color(230,230,230));
		Panel1.add(Otro);
	}

	public void Botones() {
		
		//Ingresar Conductor
		Ingresar = new JButton("Ingresar un Conductor");
		Ingresar.setBounds(120,170, 220,70);
		Ingresar.setFont(new Font("Arial", 1, 16));
		Panel1.add(Ingresar);
		
		//Buscar Conductor
		Buscar = new JButton("Buscar un Conductor");
		Buscar.setBounds(120,270,220,70);
		Buscar.setFont(new Font("Arial", 1, 16));
		Panel1.add(Buscar);
		
		//Actualizar Conductor
		Actualizar = new JButton("Actualizar un Conductor");
		Actualizar.setBounds(120,370,220,70);
		Actualizar.setFont(new Font("Arial", 1, 16));
		Panel1.add(Actualizar);
		
		//Eliminar Conductor
		Eliminar = new JButton("Eliminar un Conductor");
		Eliminar.setBounds(120,470,220,70);
		Eliminar.setFont(new Font("Arial", 1, 16));
		Panel1.add(Eliminar);
		
		//Salir
		Salir = new JButton("Salir");
		Salir.setBounds(120,570,220,70);
		Salir.setFont(new Font("Arial", 1, 16));
		Panel1.add(Salir);
		
		//Generar Reporte
		Reporte = new JButton("Generar Reporte");
		Reporte.setBounds(800,160,190,60);
		Reporte.setFont(new Font("Arial", 1, 16));
		Panel1.add(Reporte);
		
		//A�adiendo sus oyentes
		Ingresar.addActionListener(controlprincipal);
		Buscar.addActionListener(controlprincipal);
		Actualizar.addActionListener(controlprincipal);
		Eliminar.addActionListener(controlprincipal);
		Salir.addActionListener(controlprincipal);
		Reporte.addActionListener(controlprincipal);
	}
	
	//------->Panel de Ingresar<-------
	
	public void EtiquetasIngresar() {
		
		//Ingrese los siguientes datos
		Ingrese = new JLabel("Ingrese los siguientes datos");
		Ingrese.setBounds(275,5, 800, 50);
		Ingrese.setFont(new Font("Harlow Solid Italic", 1, 40));
		Ingrese.setForeground(new Color(250,250,0));
		Panel2.add(Ingrese);
		
		//1er Segmento
		
			//Datos del Conductor
			DatosConductor = new JLabel("Datos del Conductor");
			DatosConductor.setBounds(60,50, 400,50);
			DatosConductor.setFont(new Font("Arial", 0, 20));
			DatosConductor.setForeground(new Color(100,230,100));
			Panel2.add(DatosConductor);
			
			//Primera Fila
			
				//Nombre Completo
				Nombre = new JLabel("Nombres y Apellidos");
				Nombre.setBounds(120,100, 400,50);
				Nombre.setFont(new Font("Arial", 0, 18));
				Nombre.setForeground(new Color(230,230,230));
				Panel2.add(Nombre);
				
				//Cedula
				CI = new JLabel("Cedula");
				CI.setBounds(420,100, 400,50);
				CI.setFont(new Font("Arial", 0, 18));
				CI.setForeground(new Color(230,230,230));
				Panel2.add(CI);
				
				//Dirrecion
				dir = new JLabel("Direccion");
				dir.setBounds(720,100, 400,50);
				dir.setFont(new Font("Arial", 0, 18));
				dir.setForeground(new Color(230,230,230));
				Panel2.add(dir);
				
			//Segunda Fila
				
				//Telefono de contacto
				telef = new JLabel("Telefono de Contacto");
				telef.setBounds(120,200, 400,50);
				telef.setFont(new Font("Arial", 0, 18));
				telef.setForeground(new Color(230,230,230));
				Panel2.add(telef);
				
				//Correo Electronico
				correo = new JLabel("Correo Electronico");
				correo.setBounds(420,200, 400,50);
				correo.setFont(new Font("Arial", 0, 18));
				correo.setForeground(new Color(230,230,230));
				Panel2.add(correo);
				
			//Tercera Fila
				
				//Estado
				estado = new JLabel("Estado/Provincia");
				estado.setBounds(120,300, 400,50);
				estado.setFont(new Font("Arial", 0, 18));
				estado.setForeground(new Color(230,230,230));
				Panel2.add(estado);
				
				//Municipio
				municipio = new JLabel("Municipio");
				municipio.setBounds(420,300, 400,50);
				municipio.setFont(new Font("Arial", 0, 18));
				municipio.setForeground(new Color(230,230,230));
				Panel2.add(municipio);
				
		//2do Segmento
				
			//Datos del Vehiculo
			DatosVehiculo = new JLabel("Datos del Vehiculo");
			DatosVehiculo.setBounds(60,400, 400,50);
			DatosVehiculo.setFont(new Font("Arial", 0, 20));
			DatosVehiculo.setForeground(new Color(100,230,100));
			Panel2.add(DatosVehiculo);	
				
				//Primera Fila	
			
					//Placa del Vehiculo
					placa = new JLabel("Placa del Vehiculo");
					placa.setBounds(120,450, 400,50);
					placa.setFont(new Font("Arial", 0, 18));
					placa.setForeground(new Color(230,230,230));
					Panel2.add(placa);
					
					//Modelo del Vehiculo
					modelo = new JLabel("Modelo del Vehiculo");
					modelo.setBounds(420,450, 400,50);
					modelo.setFont(new Font("Arial", 0, 18));
					modelo.setForeground(new Color(230,230,230));
					Panel2.add(modelo);
					
					//Marca del Vehiculo
					marca = new JLabel("Marca del Vehiculo");
					marca.setBounds(720,450, 400,50);
					marca.setFont(new Font("Arial", 0, 18));
					marca.setForeground(new Color(230,230,230));
					Panel2.add(marca);
	}

	public void InputsIngresar() {
		
		//1er Segmento
		
			//Primera Fila
		
				//Nombre Completo
				NombreC = new JTextField(200);
				NombreC.setBounds(120,150, 200,30);
				Panel2.add(NombreC);
				
				//Cedula
				Cedula = new JTextField();
				Cedula.setBounds(420,150, 200,30);
				Panel2.add(Cedula);
				
				//Cedula
				Direccion = new JTextField(200);
				Direccion.setBounds(720,150, 200,30);
				Panel2.add(Direccion);
			
			//Segunda fila
				
				//Telefono
				Tlf = new JTextField(50);
				Tlf.setBounds(120,250, 200,30);
				Panel2.add(Tlf);
				
				//Correo Electronico
				CorreoE = new JTextField(100);
				CorreoE.setBounds(420,250, 200,30);
				Panel2.add(CorreoE);
				
			//Tercera Fila
				
				//Estado/Provincia
				Estado = new JComboBox(estados);
				Estado.setBounds(120,350, 200,30);
				Panel2.add(Estado);
				
				//Municipio
				Municipio = new JComboBox();
				Municipio.setBounds(420,350, 210,30);
				Municipio.setEnabled(false);
				Panel2.add(Municipio);
				
				Municipio.addActionListener(controlprincipal);
				Estado.addActionListener(controlprincipal);
				
		//2do Segmento
			
			//Primera Fila
				
				//Placa del vehiculo
				PlacaVe = new JTextField(12);
				PlacaVe.setBounds(120,500, 200,30);
				Panel2.add(PlacaVe);
				
				//Modelo del vehiculo
				ModeloVe = new JTextField(50);
				ModeloVe.setBounds(420,500, 200,30);
				Panel2.add(ModeloVe);
				
				//Placa del vehiculo
				MarcaVe = new JTextField(50);
				MarcaVe.setBounds(720,500, 200,30);
				Panel2.add(MarcaVe);
						
		//Acciones para limitar la cantidad de caracteres
		
		//Llamamos al controlador principal
				
		NombreC.addKeyListener((KeyListener) controlprincipal);
		Direccion.addKeyListener((KeyListener) controlprincipal);
		Tlf.addKeyListener((KeyListener) controlprincipal);
		CorreoE.addKeyListener((KeyListener) controlprincipal);
		Estado.addKeyListener((KeyListener) controlprincipal);
		Municipio.addKeyListener((KeyListener) controlprincipal);
		PlacaVe.addKeyListener((KeyListener) controlprincipal);
		ModeloVe.addKeyListener((KeyListener) controlprincipal);
		MarcaVe.addKeyListener((KeyListener) controlprincipal);
	}

	public void BotonesIngresar() {
		
		//Enviar
		Enviar = new JButton("Enviar");
		Enviar.setBounds(425,580,200,70);
		Enviar.setFont(new Font("Arial", 1, 20));
		Panel2.add(Enviar);
		
		//Regresar
		Regresar = new JButton("Regresar");
		Regresar.setBounds(5,5,90,40);
		Regresar.setFont(new Font("Arial", 1, 12));
		Panel2.add(Regresar);
		
		//Accion con los botones
		Enviar.addActionListener(controlprincipal);
		Regresar.addActionListener(controlprincipal); //Se le aplica un oyente
	}
	
	//------->Panel de Buscar<-------
	
	public void EtiquetasBusqueda() {
		
		//Busqueda de un conductor
		Busqueda = new JLabel("Busqueda de un conductor");
		Busqueda.setBounds(330,5,800,50);
		Busqueda.setFont(new Font("Harlow Solid Italic", 1, 40));
		Busqueda.setForeground(new Color(250,250,0));
		Panel3.add(Busqueda);
		
		//Ingrese la C.I del conductor
		IngreseCI = new JLabel("Ingrese la C.I del conductor");
		IngreseCI.setBounds(100,150,400,30);
		IngreseCI.setFont(new Font("Arial", 0, 20));
		IngreseCI.setForeground(new Color(100,230,100));
		Panel3.add(IngreseCI);
	}
	
	public void InputBusqueda() {
		
		//Campo para la cedula - Segmento Buscar
		InputBus = new JTextField();
		InputBus.setBounds(100,190,245,30);
		Panel3.add(InputBus);
	}
	
	public void BotonesBusqueda() {
		//Buscar
		BuscarConductor = new JButton("Buscar");
		BuscarConductor.setBounds(360, 180, 150, 50);
		BuscarConductor.setFont(new Font("Arial", 1,20));
		Panel3.add(BuscarConductor);
		
		//Consulta General
		ConsultaGeneral = new JButton("Consulta General");
		ConsultaGeneral.setBounds(750, 180, 200, 50);
		ConsultaGeneral.setFont(new Font("Arial", 1,20));
		Panel3.add(ConsultaGeneral);
		
		//Regresar
		Regresar2 = new JButton("Regresar");
		Regresar2.setBounds(5,5,90,40);
		Regresar2.setFont(new Font("Arial", 1, 12));
		Panel3.add(Regresar2);
		
		//Accion para los botones
		BuscarConductor.addActionListener(controlprincipal);		
		ConsultaGeneral.addActionListener(controlprincipal);
		Regresar2.addActionListener(controlprincipal); //Se le aplica un oyente
	}
	
	public void TablaBusqueda() {
		
		Area = new JTextArea();
		Area.setBounds(150,300,800,340);
		Area.setFont(new Font("Calibri", 1, 17));
		Area.setEditable(false);
		Area.setText("");
		Panel3.add(Area);
		
		JScrollPane scroll = new JScrollPane(Area);
		scroll.setBounds(150,300,800,340);
		Panel3.add(scroll);
	}

	//------->Panel de Actualizar<-------
	
	public void EtiquetasActualizar() {
		
		//Actualizar
		Actualizacion = new JLabel("Actualizar datos de un conductor");
		Actualizacion.setBounds(275,5,800,50);
		Actualizacion.setFont(new Font("Harlow Solid Italic", 1, 40));
		Actualizacion.setForeground(new Color(250,250,0));
		Panel4.add(Actualizacion);
		
		//Ingrese la C.I del conductor - Segmento Actualizar
		IngreseCI2 = new JLabel("Ingrese la C.I del conductor");
		IngreseCI2.setBounds(100,150,400,30);
		IngreseCI2.setFont(new Font("Arial", 0, 20));
		IngreseCI2.setForeground(new Color(100,230,100));
		Panel4.add(IngreseCI2);
		
		//Datos del conductor
		
		DatosConductorAc = new JLabel("Datos del conductor");
		DatosConductorAc.setBounds(75,260, 400, 50);
		DatosConductorAc.setFont(new Font("Arial", 0, 20));
		DatosConductorAc.setForeground(new Color(100,230,100));
		
		NombreAc = new JLabel("Nombres y apellidos");
		NombreAc.setBounds(100, 300, 400,50);
		NombreAc.setFont(new Font("Arial", 0, 18));
		NombreAc.setForeground(new Color(230,230,230));
		
		CIAc = new JLabel("Cedula");
		CIAc.setBounds(350,300,400,50);
		CIAc.setFont(new Font("Arial", 0, 18));
		CIAc.setForeground(new Color(230,230,230));
		
		dirAc = new JLabel("Direccion");
		dirAc.setBounds(550,300,400,50);
		dirAc.setFont(new Font("Arial", 0, 18));
		dirAc.setForeground(new Color(230,230,230));
		
		telefAc = new JLabel("Telefono");
		telefAc.setBounds(750,300,400,50);
		telefAc.setFont(new Font("Arial", 0, 18));
		telefAc.setForeground(new Color(230,230,230));
		
		correoAc = new JLabel("Correo electronico");
		correoAc.setBounds(100, 400,400,50);
		correoAc.setFont(new Font("Arial", 0, 18));
		correoAc.setForeground(new Color(230,230,230));
		
		estadoAc = new JLabel("Estado");
		estadoAc.setBounds(350,400,400,50);
		estadoAc.setFont(new Font("Arial", 0, 18));
		estadoAc.setForeground(new Color(230,230,230));
		
		municipioAc = new JLabel("Municipio");
		municipioAc.setBounds(550,400,400,50);
		municipioAc.setFont(new Font("Arial", 0, 18));
		municipioAc.setForeground(new Color(230,230,230));
		
		//Datos del vehiculo
		
		DatosVehiculoAc = new JLabel("Datos del vehiculo");
		DatosVehiculoAc.setBounds(75, 500, 400,50);
		DatosVehiculoAc.setFont(new Font("Arial", 0, 20));
		DatosVehiculoAc.setForeground(new Color(100,230,100));
		
		placaAc = new JLabel("Placa");
		placaAc.setBounds(100,540,400,50);
		placaAc.setFont(new Font("Arial", 0, 18));
		placaAc.setForeground(new Color(230,230,230));
		
		modeloAc = new JLabel("Modelo");
		modeloAc.setBounds(350, 540,400,50);
		modeloAc.setFont(new Font("Arial", 0, 18));
		modeloAc.setForeground(new Color(230,230,230));
		
		marcaAc = new JLabel("Marca");
		marcaAc.setBounds(550, 540,400,50);
		marcaAc.setFont(new Font("Arial", 0, 18));
		marcaAc.setForeground(new Color(230,230,230));
		
		//A�adiendolos al panel las etiquetas
		Panel4.add(DatosConductorAc);
		Panel4.add(NombreAc);
		Panel4.add(DatosVehiculoAc);
		Panel4.add(CIAc);
		Panel4.add(dirAc);
		Panel4.add(telefAc);
		Panel4.add(correoAc);
		Panel4.add(estadoAc);
		Panel4.add(municipioAc);
		Panel4.add(placaAc);
		Panel4.add(modeloAc);
		Panel4.add(marcaAc);
	}

	public void InputActualizar() {
		
		//Campo para la cedula - Segmento Actualizar
		InputAct = new JTextField();
		InputAct.setBounds(100,190,245,30);
		Panel4.add(InputAct);
		
		//Declarando posicion de los inputs
		NombreCAc = new JTextField();
		NombreCAc.setBounds(100, 350, 200,30);
		
		CedulaAc = new JTextField();
		CedulaAc.setBounds(350, 350, 100, 30);
		
		DireccionAc = new JTextField();
		DireccionAc.setBounds(550,350,150,30);
		
		TlfAc = new JTextField();
		TlfAc.setBounds(750,350,150,30);
		
		CorreoEAc = new JTextField();
		CorreoEAc.setBounds(100,450,200,30);
		
		ListaEstadoAc = new JComboBox(estados);
		ListaEstadoAc.setBounds(350,450,150,30);
		
		
		ListaMunicipioAc = new JComboBox();
		ListaMunicipioAc.setBounds(550,450,210,30);
		ListaMunicipioAc.setEnabled(false);
		
		PlacaVeAc = new JTextField();
		PlacaVeAc.setBounds(100,590,200,30);
		
		ModeloVeAc = new JTextField();
		ModeloVeAc.setBounds(350,590,150,30);
		
		MarcaVeAc = new JTextField();
		MarcaVeAc.setBounds(550,590,150,30);
		
		AreaAct = new JTextArea();
		AreaAct.setBounds(550,90,500,190);
		AreaAct.setEditable(false);
		
		//A�adiendolos al panel los inputs
		Panel4.add(NombreCAc);
		Panel4.add(CedulaAc);
		Panel4.add(DireccionAc);
		Panel4.add(TlfAc);
		Panel4.add(CorreoEAc);
		Panel4.add(ListaEstadoAc);
		Panel4.add(ListaMunicipioAc);
		Panel4.add(PlacaVeAc);
		Panel4.add(ModeloVeAc);
		Panel4.add(MarcaVeAc);
		Panel4.add(AreaAct);
		
		//A�adiendo listener
		ListaEstadoAc.addActionListener(controlprincipal);
		PlacaVeAc.addKeyListener(controlprincipal);
	}

	public void BotonesActualizar() {
		
		//Buscar para Actualizar
		BuscarAct = new JButton("Buscar");
		BuscarAct.setBounds(360, 180, 150, 50);
		BuscarAct.setFont(new Font("Arial", 1,20));
		Panel4.add(BuscarAct);
		
		//Regresar
		Regresar3 = new JButton("Regresar");
		Regresar3.setBounds(5,5,90,40);
		Regresar3.setFont(new Font("Arial", 1, 12));
		Panel4.add(Regresar3);
		
		//Enviar
		ActualizarAc = new JButton("Actualizar");
		ActualizarAc.setFont(new Font("Arial", 1, 20));
		ActualizarAc.setBounds(775,580, 150,50);
		Panel4.add(ActualizarAc);
		
		//Accion para los botones
		BuscarAct.addActionListener(controlprincipal);		
		Regresar3.addActionListener(controlprincipal); //Se le aplica un oyente	
		ActualizarAc.addActionListener(controlprincipal);
	}
	
	//------->Panel de Borrar<-------
	
	public void EtiquetasBorrar() {
		
		//Borrar
		BorrarCo = new JLabel("Borrar a un conductor");
		BorrarCo.setBounds(360,5,820,50);
		BorrarCo.setFont(new Font("Harlow Solid Italic", 1, 40));
		BorrarCo.setForeground(new Color(250,250,0));
		Panel5.add(BorrarCo);
		
		//Ingrese la C.I del conductor - Segmento Borrar
		IngreseCI3 = new JLabel("Ingrese la C.I del conductor");
		IngreseCI3.setBounds(100,150,400,30);
		IngreseCI3.setFont(new Font("Arial", 0, 20));
		IngreseCI3.setForeground(new Color(100,230,100));
		Panel5.add(IngreseCI3);
		
		//Pregunta de seguridad
		DeseaBor = new JLabel("�Desea eliminar este conductor?");
		DeseaBor.setBounds(575,90,700,30);
		DeseaBor.setFont(new Font("Arial", 1, 28));
		DeseaBor.setForeground(new Color(100,230,100));
		DeseaBor.setVisible(false);
		Panel5.add(DeseaBor);
	}	

	public void InputBorrar() {
		
		//Campo para la cedula - Segmento Borrar
		InputBor= new JTextField();
		InputBor.setBounds(100,190,245,30);
		Panel5.add(InputBor);
		
		AreaBor = new JTextArea();
		AreaBor.setBounds(150,300,800,340);
		AreaBor.setFont(new Font("Calibri", 1, 17));
		AreaBor.setEditable(false);
		Panel5.add(AreaBor);
	}

	public void BotonesBorrar() {
		
		//Buscar para borrar
		BuscarBor = new JButton("Buscar");
		BuscarBor.setBounds(360, 180, 150, 50);
		BuscarBor.setFont(new Font("Arial", 1,20));
		Panel5.add(BuscarBor);
		
		//Regresar
		Regresar4 = new JButton("Regresar");
		Regresar4.setBounds(5,5,90,40);
		Regresar4.setFont(new Font("Arial", 1, 12));
		Panel5.add(Regresar4);
		
		//Boton para afirmar
		Afirmar = new JButton("S�");
		Afirmar.setBounds(650,180,100,50);
		Afirmar.setFont(new Font("Arial", 1, 20));
		Afirmar.setEnabled(false);
		Afirmar.setVisible(false);
		Panel5.add(Afirmar);
		
		//Boton para negar
		Negar = new JButton("No");
		Negar.setBounds(850,180,100,50);
		Negar.setFont(new Font("Arial", 1, 20));
		Negar.setEnabled(false);
		Negar.setVisible(false);
		Panel5.add(Negar);
		
		//Accion para los botones
		BuscarBor.addActionListener(controlprincipal);		
		Regresar4.addActionListener(controlprincipal);
		Afirmar.addActionListener(controlprincipal);
		Negar.addActionListener(controlprincipal);
	}
}