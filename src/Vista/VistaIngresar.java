package Vista;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyListener;

import javax.swing.*;

import Controlador.ControladorIngresar;

public class VistaIngresar extends JPanel{

	//Componentes
	public JLabel Ingrese, DatosConductor, DatosVehiculo, Nombre, CI, dir, telef, correo, estado,municipio, placa, modelo, marca;
	public JTextField NombreC, Cedula,Direccion,Tlf,CorreoE,Estado,Municipio,PlacaVe,ModeloVe,MarcaVe;
	public JButton Enviar, Regresar;
	
	//Instancia con el controlador
	ControladorIngresar controladoringresar = new ControladorIngresar(this);
	
	public VistaIngresar() {
		
		setLayout(null);
		setBackground(new Color(87,35,100));
		
		EtiquetasIngresar();
		InputsIngresar();
		BotonesIngresar();
	}
	
	public void EtiquetasIngresar() {
		
		//Ingrese los siguientes datos
		Ingrese = new JLabel("Ingrese los siguientes datos");
		Ingrese.setBounds(275,5, 800, 50);
		Ingrese.setFont(new Font("Harlow Solid Italic", 1, 40));
		Ingrese.setForeground(new Color(250,250,0));
		add(Ingrese);
		
		//1er Segmento
		
			//Datos del Conductor
			DatosConductor = new JLabel("Datos del Conductor");
			DatosConductor.setBounds(60,50, 400,50);
			DatosConductor.setFont(new Font("Arial", 0, 20));
			DatosConductor.setForeground(new Color(100,230,100));
			add(DatosConductor);
			
			//Primera Fila
			
				//Nombre Completo
				Nombre = new JLabel("Nombres y Apellidos");
				Nombre.setBounds(120,100, 400,50);
				Nombre.setFont(new Font("Arial", 0, 18));
				Nombre.setForeground(new Color(230,230,230));
				add(Nombre);
				
				//Cedula
				CI = new JLabel("Cedula");
				CI.setBounds(420,100, 400,50);
				CI.setFont(new Font("Arial", 0, 18));
				CI.setForeground(new Color(230,230,230));
				add(CI);
				
				//Dirrecion
				dir = new JLabel("Direccion");
				dir.setBounds(720,100, 400,50);
				dir.setFont(new Font("Arial", 0, 18));
				dir.setForeground(new Color(230,230,230));
				add(dir);
				
			//Segunda Fila
				
				//Telefono de contacto
				telef = new JLabel("Telefono de Contacto");
				telef.setBounds(120,200, 400,50);
				telef.setFont(new Font("Arial", 0, 18));
				telef.setForeground(new Color(230,230,230));
				add(telef);
				
				//Correo Electronico
				correo = new JLabel("Correo Electronico");
				correo.setBounds(420,200, 400,50);
				correo.setFont(new Font("Arial", 0, 18));
				correo.setForeground(new Color(230,230,230));
				add(correo);
				
			//Tercera Fila
				
				//Estado
				estado = new JLabel("Estado/Provincia");
				estado.setBounds(120,300, 400,50);
				estado.setFont(new Font("Arial", 0, 18));
				estado.setForeground(new Color(230,230,230));
				add(estado);
				
				//Municipio
				municipio = new JLabel("Municipio");
				municipio.setBounds(420,300, 400,50);
				municipio.setFont(new Font("Arial", 0, 18));
				municipio.setForeground(new Color(230,230,230));
				add(municipio);
				
		//2do Segmento
				
			//Datos del Vehiculo
			DatosVehiculo = new JLabel("Datos del Vehiculo");
			DatosVehiculo.setBounds(60,400, 400,50);
			DatosVehiculo.setFont(new Font("Arial", 0, 20));
			DatosVehiculo.setForeground(new Color(100,230,100));
			add(DatosVehiculo);	
				
				//Primera Fila	
			
					//Placa del Vehiculo
					placa = new JLabel("Placa del Vehiculo");
					placa.setBounds(120,450, 400,50);
					placa.setFont(new Font("Arial", 0, 18));
					placa.setForeground(new Color(230,230,230));
					add(placa);
					
					//Modelo del Vehiculo
					modelo = new JLabel("Modelo del Vehiculo");
					modelo.setBounds(420,450, 400,50);
					modelo.setFont(new Font("Arial", 0, 18));
					modelo.setForeground(new Color(230,230,230));
					add(modelo);
					
					//Marca del Vehiculo
					marca = new JLabel("Marca del Vehiculo");
					marca.setBounds(720,450, 400,50);
					marca.setFont(new Font("Arial", 0, 18));
					marca.setForeground(new Color(230,230,230));
					add(marca);
	}

	public void InputsIngresar() {
		
		//1er Segmento
		
			//Primera Fila
		
				//Nombre Completo
				NombreC = new JTextField(200);
				NombreC.setBounds(120,150, 200,30);
				add(NombreC);
				
				//Cedula
				Cedula = new JTextField();
				Cedula.setBounds(420,150, 200,30);
				add(Cedula);
				
				//Cedula
				Direccion = new JTextField(200);
				Direccion.setBounds(720,150, 200,30);
				add(Direccion);
			
			//Segunda fila
				
				//Telefono
				Tlf = new JTextField(50);
				Tlf.setBounds(120,250, 200,30);
				add(Tlf);
				
				//Correo Electronico
				CorreoE = new JTextField(100);
				CorreoE.setBounds(420,250, 200,30);
				add(CorreoE);
				
			//Tercera Fila
				
				//Estado/Provincia
				Estado = new JTextField(50);
				Estado.setBounds(120,350, 200,30);
				add(Estado);
				
				//Municipio
				Municipio = new JTextField(50);
				Municipio.setBounds(420,350, 200,30);
				add(Municipio);	
				
		//2do Segmento
			
			//Primera Fila
				
				//Placa del vehiculo
				PlacaVe = new JTextField(12);
				PlacaVe.setBounds(120,500, 200,30);
				add(PlacaVe);
				
				//Modelo del vehiculo
				ModeloVe = new JTextField(50);
				ModeloVe.setBounds(420,500, 200,30);
				add(ModeloVe);
				
				//Placa del vehiculo
				MarcaVe = new JTextField(50);
				MarcaVe.setBounds(720,500, 200,30);
				add(MarcaVe);
						
		//Acciones para limitar la cantidad de caracteres
		
		//Llamamos al controlador principal
				
		NombreC.addKeyListener((KeyListener) controladoringresar);
		Direccion.addKeyListener((KeyListener) controladoringresar);
		Tlf.addKeyListener((KeyListener) controladoringresar);
		CorreoE.addKeyListener((KeyListener) controladoringresar);
		Estado.addKeyListener((KeyListener) controladoringresar);
		Municipio.addKeyListener((KeyListener) controladoringresar);
		PlacaVe.addKeyListener((KeyListener) controladoringresar);
		ModeloVe.addKeyListener((KeyListener) controladoringresar);
		MarcaVe.addKeyListener((KeyListener) controladoringresar);
	}

	public void BotonesIngresar() {
		
		//Enviar
		Enviar = new JButton("Enviar");
		Enviar.setBounds(425,580,200,70);
		Enviar.setFont(new Font("Arial", 1, 20));
		add(Enviar);
		
		//Regresar
		Regresar = new JButton("Regresar");
		Regresar.setBounds(5,5,90,40);
		Regresar.setFont(new Font("Arial", 1, 12));
		add(Regresar);
		
		//Accion con los botones
		Enviar.addActionListener(controladoringresar);
		Regresar.addActionListener(controladoringresar);
	}
}
